package ru.baykalapps.base;


import java.io.File;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class Generetor {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "ru.iappstee.sinax.model");

        Entity chatMessage = schema.addEntity("ChatMessage");
        chatMessage.addIdProperty();
        chatMessage.addStringProperty("email");
        chatMessage.addStringProperty("avatar");
        chatMessage.addStringProperty("username");
        chatMessage.addStringProperty("text");
        chatMessage.addDateProperty("date");
        chatMessage.addBooleanProperty("admin");
        chatMessage.addBooleanProperty("justDate");

        Entity note = schema.addEntity("Note");
        note.addIdProperty().autoincrement();
        note.addLongProperty("created");
        note.addStringProperty("note");

        Entity category = schema.addEntity("Category");
        category.addIdProperty().autoincrement();
        category.addLongProperty("title");
        category.addStringProperty("text");


        Entity contact = schema.addEntity("Contact");
        contact.addIdProperty();
        contact.addStringProperty("type");

        Entity contactRow = schema.addEntity("ContactRow");
        contactRow.addIdProperty().autoincrement();
        contactRow.addStringProperty("value");
        contactRow.addStringProperty("title");

        Property contactId = contactRow.addLongProperty("contactId").notNull().getProperty();
        ToMany contactToContactRow = contact.addToMany(contactRow, contactId);
        contactToContactRow.setName("rows");


        File f = new File("daogenerator/generated");
        if (!f.exists())
            f.mkdirs();

        for (File oldFile : f.listFiles()) {
            oldFile.delete();
        }

        System.out.print(f.getAbsoluteFile());

        new DaoGenerator().generateAll(schema, f.getAbsolutePath());
    }
}
