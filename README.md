alias: sinax/p:sinax2015


Spanded hours: https://docs.google.com/spreadsheets/d/1xSb1NhKnwdPmEawpaS0NwK5FDYgXJ8Cqd6meMUv3eTs/edit#gid=0

сервер для запросов http://www.cardadmin.ru/
http://www.cardadmin.ru/api/v1/rev/sinaxru/ и т.д.

номер ревизии, если в любом разделе изменилась информация ревизия меняется
/api/v1/rev/<appname>/


сбор устройств для пушей 
/api/v1/device/<appname>/ - POST
параметры: uid  imei
возврашает: {'status': 'update'}  {'status': 'none'}


регистрация пользователя
/api/v1/register/ - POST
параметры: mail  username  password
возврашает: {'status': 'mail_exists'}  {'status': 'username_exists'}  {'status': 'ok', 'auth_token': user_data.token}  {'status': 'none'}


авторизация пользователя 
/api/v1/auth/ - POST
параметры: mail  password  appname
возврашает: {'status': 'user_not_found'}  {'status': 'password_invalid'}  {'status': 'ok', 'auth_token': user_data.token, 'auth_status': user_data.status, 'auth_name': user_data.name, 'auth_admin': user_data.admin, 'auth_avatar': user_data.avatar}  {'status': 'none'}


отправка сообщения 
/api/v1/send-message/<appname>/ - POST
параметры: mail  token  text
возврашает: {'status': 'auth_invalid'}  {'status': 'success', 'id': str(message.oid)}  {'status': 'none'}


список сообщений
/api/v1/messages/<appname>/
параметры: user_mail - для выборки по пользователю, для всех без параметров 
возврашает: {'status': 'ok', 'messages': mess_arr}   {'status': 'none'}


обнуляет счетчик не прочитанных сообщений, возможно не понадобится в андроид версии
/api/v1/messages-read/ - POST
параметры: uid  imei
возврашает: {'status': 'clean'}  {'status': 'none'}


список стандартных аватаров
/api/v1/avatars/
возврашает: []


загрузка кастомной фото для аватара пользователя 
/api/v1/avatar/<user_mail>/ - POST
параметры: file
возврашает: {'avatar': user_data.avatar}  {'status': 'badfile'}  {'status': 'none'}


контактная информация (формат немного странный, так было нужно для ios) 
/api/v1/info/<appname>/


кол-во установленных копий + зарег. пользователей 
/api/v1/users/<appname>/
{'users': str(len(ids)), 'install': str(dev_data)}


фото
/api/v1/images/<appname>/


категории
/api/v1/category-list/<appname>/


html конкретной категории
/api/v1/category/<cid>/