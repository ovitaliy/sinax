package ru.baykalapps.base.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;

/**
 * Created by ovitali on 06.07.2015.
 * Project is Sinax
 */
public class MainButton extends RelativeLayout {

    private int mColor = Color.WHITE;
    private int mPressedColor;

    private TextView mLabel;
    private ImageButton mIcon;

    public MainButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (SinaxApplication.THEME == 1) {
            mColor = SinaxApplication.GREY_COLOR;
        }

        mPressedColor = getResources().getColor(R.color.main_button_pressed);

        View.inflate(context, R.layout.item_main_button, this);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MainButton);

        mLabel = ((TextView) findViewById(R.id.label));
        mLabel.setClickable(false);
        mLabel.setText(a.getString(R.styleable.MainButton_text));
        mIcon = ((ImageButton) findViewById(R.id.icon));
        mIcon.setClickable(false);
        mIcon.setImageDrawable(a.getDrawable(R.styleable.MainButton_iconSrc));
        a.recycle();

        setClickable(true);
        setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                    changePressState(true);
                } else {
                    changePressState(false);
                }
                return false;
            }

        });

        changePressState(false);
    }

    private void changePressState(boolean isPressed) {
        mLabel.setTextColor(isPressed ? mPressedColor : mColor);
        mIcon.setColorFilter(isPressed ? mPressedColor : mColor);
    }

    public void changeColor(int color) {
        mColor = color;
        changePressState(false);
    }
}
