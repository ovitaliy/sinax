package ru.baykalapps.base.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.activities.FullScreenImageActivity;
import ru.baykalapps.base.db.dao.ContactDao;
import ru.baykalapps.base.db.dao.ContactRowDao;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.utils.ConverterUtil;
import ru.baykalapps.base.utils.UiUtil;

/**
 * Created by ovitali on 25.07.2015.
 * Project is Sinax
 */
public class PhotoCardLayout extends ProportionLinearLayout implements View.OnClickListener {
    public PhotoCardLayout(Context context) {
        super(context);
    }

    public PhotoCardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    ViewPager mViewPager;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        findViewById(R.id.call_btn).setOnClickListener(this);
        findViewById(R.id.email_btn).setOnClickListener(this);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new Adapter());
        mViewPager.setCurrentItem(SinaxApplication.PHOTO_CARDS.length / 2);

        TextView logo = ((TextView) findViewById(R.id.logo));
        logo.setText(SinaxApplication.MAIN_LABEL);
        logo.setShadowLayer(ConverterUtil.dpToPix(getContext(), 5), 0, 3, Color.BLACK);

        invalidateForTheme();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.call_btn) {
            call("phone", "tel:");

        } else if (i == R.id.email_btn) {
            call("email", "mailto:");

        } else {
            FullScreenImageActivity.startNewInstance(getContext(), (String) v.getTag());
        }
    }

    private void call(String type, String prefix) {
        try {
            List<Contact> contactList = SinaxApplication.getDatabaseDao().getContactDao().queryBuilder().where(ContactDao.Properties.Type.eq(type)).list();
            List<ContactRow> contactRowList = SinaxApplication.getDatabaseDao().getContactRowDao().queryBuilder().where(ContactRowDao.Properties.ContactId.eq(contactList.get(0).getId())).list();
            UiUtil.openLink(getContext(), prefix + contactRowList.get(0).getValue());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void invalidateForTheme() {
        if (SinaxApplication.THEME == -1) {
            mViewPager.setPadding(0, 0, 0, 0);
            mViewPager.setBackgroundResource(0);
        } else {
            int padding = (int) ConverterUtil.dpToPix(getContext(), 10);
            mViewPager.setPadding(padding, padding, padding, padding);
            mViewPager.setBackgroundResource(android.R.drawable.dialog_holo_light_frame);
        }
    }

    public void reload() {
        mViewPager.getAdapter().notifyDataSetChanged();
        mViewPager.setCurrentItem(SinaxApplication.PHOTO_CARDS.length / 2);
    }


    private class Adapter extends PagerAdapter {
        @Override
        public int getCount() {
            return SinaxApplication.PHOTO_CARDS.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(getContext());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            String url = SinaxApplication.PHOTO_CARDS[position].getPath();

            ImageLoader.getInstance().displayImage(
                    url,
                    imageView
            );

            imageView.setTag(String.valueOf(position));
            container.addView(imageView, 0);
            imageView.setOnClickListener(PhotoCardLayout.this);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }
}
