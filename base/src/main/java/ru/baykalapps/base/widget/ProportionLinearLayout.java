package ru.baykalapps.base.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import ru.baykalapps.base.R;

/**
 * Created by ovitali on 07.07.2015.
 * Project is Sinax
 */
public class ProportionLinearLayout extends RelativeLayout {

    private float mProportion = 0.667f;

    public ProportionLinearLayout(Context context) {
        super(context);
    }

    public ProportionLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProportionLinearLayout);
        mProportion = a.getFloat(R.styleable.ProportionLinearLayout_proportion,0.667f);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        float height = (float) MeasureSpec.getSize(widthMeasureSpec) * mProportion;

        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((int) height, MeasureSpec.EXACTLY));
    }
}
