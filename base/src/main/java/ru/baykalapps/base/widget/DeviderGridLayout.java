package ru.baykalapps.base.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;

/**
 * Created by ovitali on 07.07.2015.
 * Project is Sinax
 */
public class DeviderGridLayout extends FrameLayout {

    private Paint mPaint;

    int mItemMargin;

    int width = 0;

    public DeviderGridLayout(Context context) {
        this(context, null);
    }

    public DeviderGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        width = (int) (SinaxApplication.SCREEN_WIDTH * 0.7d);

        mPaint = new Paint();
        mPaint.setColor(context.getResources().getColor(R.color.dividerColor));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(2);

        setWillNotDraw(false);

        mItemMargin = (int) getResources().getDimension(R.dimen.divider_grid_item_margin);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        int cols = 2;
        int rows = (int) Math.ceil(getChildCount() / 2.0f);

        int height = width / 2 * rows;

        int size = width / cols;

        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            ViewGroup.LayoutParams lp = v.getLayoutParams();
            lp.width = size - mItemMargin * 2;
            lp.height = size - mItemMargin * 2;
            v.setX(size * (i % 2) + mItemMargin);
            v.setY(size * (i / 2) + mItemMargin);
        }

        widthSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        heightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthSpec, heightSpec);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        int cols = 2;
        int rows = (int) Math.ceil(getChildCount() / 2.0f);

        ViewGroup.LayoutParams lp = (ViewGroup.LayoutParams) getLayoutParams();
        float margin = 0;

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        int lineStep = width / cols;
        int colsStep = height / rows;

        for (int i = 1; i < rows; i++) {
            canvas.drawLine(margin, lineStep * i, width - margin, lineStep * i, mPaint);
        }

        for (int i = 1; i < cols; i++) {
            canvas.drawLine(colsStep * i, margin, colsStep * i, height - margin, mPaint);
        }

        super.draw(canvas);
    }
}
