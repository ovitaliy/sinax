package ru.baykalapps.base.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import ru.baykalapps.base.R;
import ru.baykalapps.base.utils.TypefaceCache;

public class FontTextView extends TextView {
    public FontTextView(Context context) {
        this(context, null);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode())
            return;

        String fontName = "GillSans";
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Localized);
        if (a.hasValue(R.styleable.Localized_font)) {
            fontName = a.getString(R.styleable.Localized_font);
            Typeface typeface = TypefaceCache.getTypeface(context, fontName);
            setTypeface(typeface);
        } else {
            Typeface typeface = TypefaceCache.getTypeface(context, fontName);
            setTypeface(typeface);
        }

        setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);

        a.recycle();

    }
}
