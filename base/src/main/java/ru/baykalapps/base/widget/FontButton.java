package ru.baykalapps.base.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.utils.TypefaceCache;

public class FontButton extends Button {
    public FontButton(Context context) {
        this(context, null);
    }

    public FontButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public FontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode()) return;

        String fontName = Constant.DEFAULT_FONT;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Localized);
        if (a.hasValue(R.styleable.Localized_font)) {
            fontName = a.getString(R.styleable.Localized_font);
            Typeface typeface = TypefaceCache.getTypeface(context, fontName);
            setTypeface(typeface);
        } else {
            Typeface typeface = TypefaceCache.getTypeface(context, fontName);
            setTypeface(typeface);
        }
        a.recycle();
    }
}
