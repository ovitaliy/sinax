package ru.baykalapps.base.adapter.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ItemPaddingDecoration extends RecyclerView.ItemDecoration {

    private int padding;

    public ItemPaddingDecoration(int padding) {
        this.padding = padding;
    }

    public ItemPaddingDecoration(float padding) {
        this.padding = (int) padding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = padding;

    }
}
