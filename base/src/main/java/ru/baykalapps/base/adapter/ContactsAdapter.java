package ru.baykalapps.base.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.utils.ConverterUtil;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Contact> mContacts;
    private View.OnClickListener mOnContactClick;

    private final String BASE_URL;

    private int mContactRowBackgroundColor = -1;
    private static int sMinHeight = (int) ConverterUtil.dpToPix(Resources.getSystem(), 54);

    public ContactsAdapter(List<Contact> contacts, View.OnClickListener onClickListener) {
        mContacts = contacts;
        mOnContactClick = onClickListener;

        BASE_URL = "assets://contacts/"
                + (SinaxApplication.THEME == 1 ? "light/" : "dark/");

    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_photo_card, parent, false);
            return new HeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (position > 0) {
            ViewHolder holder = (ViewHolder) viewHolder;
            Context context = holder.title.getContext();

            Contact contact = mContacts.get(position - 1);

            ImageLoader.getInstance().displayImage(
                    BASE_URL + contact.getType() + ".png",
                    holder.icon
            );

            int contactTitle = context.getResources().getIdentifier("contacts_" + contact.getType(), "string", context.getPackageName());
            holder.title.setText(contactTitle);

            holder.holder.removeAllViewsInLayout();

            List<ContactRow> contactRows = contact.getRows();
            int n = contactRows.size();
            for (int i = 0; i < n; i++) {
                ContactRow row = contactRows.get(i);

                TextView rowView = (TextView) LayoutInflater.from(context).inflate(R.layout.item_contact_row, holder.holder, false);
                rowView.setGravity(Gravity.CENTER_VERTICAL);
                rowView.setTag(R.string.contact_value, row.getValue());
                rowView.setTag(R.string.contact_type, contact.getType());
                rowView.setOnClickListener(mOnContactClick);

                if (SinaxApplication.THEME == 1) {
                    rowView.setTextColor(SinaxApplication.GREY_COLOR);
                }

                if (i < n - 1)
                    rowView.setBackgroundResource(R.drawable.bottom_line_bg);

                if (n > 1)
                    rowView.setMinHeight(sMinHeight);

                String text = row.getValue();
                if (row.getTitle() != null) {
                    if (SinaxApplication.THEME == 1) {
                        text = "<font color=\"black\">" + row.getTitle() + "</font><br/>" + text;
                    } else {
                        text = row.getTitle() + "<br/>" + text;
                    }
                }
                rowView.setText(Html.fromHtml(text));

                holder.holder.addView(rowView);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mContacts.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView title;
        private LinearLayout holder;

        public ViewHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(R.id.icon);
            title = (TextView) itemView.findViewById(R.id.title);

            holder = (LinearLayout) itemView.findViewById(R.id.contacstHolder);

            if (SinaxApplication.THEME == 1) {
                if (mContactRowBackgroundColor == -1) {
                    mContactRowBackgroundColor = itemView.getContext().getResources().getColor(R.color.contact_row_bg_light);
                }
                itemView.setBackgroundColor(mContactRowBackgroundColor);
            }
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
}
