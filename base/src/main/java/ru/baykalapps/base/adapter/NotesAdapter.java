package ru.baykalapps.base.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.swipe.BaseSwipeAdapter;
import com.malinskiy.superrecyclerview.swipe.SwipeLayout;

import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.events.NoteEvent;
import ru.baykalapps.base.model.Note;

/**
 * Created by ovitali on 07.07.2015.
 * Project is Sinax
 */
public class NotesAdapter extends BaseSwipeAdapter<NotesAdapter.ViewHolder> {

    private List<Note> mNotes;

    private View.OnClickListener onClickListener;

    public NotesAdapter(List<Note> list) {
        mNotes = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_note, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Note note = mNotes.remove(viewHolder.getLayoutPosition());
                EventBus.getDefault().post(new NoteEvent(NoteEvent.EventType.DELETE, note.getId()));
                notifyDataSetChanged();
                SinaxApplication.getDatabaseDao().getNoteDao().delete(note);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Note n = mNotes.get(i);
        viewHolder.dateView.setText(Constant.DATE_FORMAT.NOTE.format(new Date(n.getCreated())));
        viewHolder.noteView.setText(n.getNote());
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    class ViewHolder extends BaseSwipeAdapter.BaseSwipeableViewHolder {

        TextView dateView;
        TextView noteView;

        public ViewHolder(View itemView) {
            super(itemView);

            swipeLayout.setDragEdge(SwipeLayout.DragEdge.Right);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

            dateView = (TextView) itemView.findViewById(R.id.username);
            noteView = (TextView) itemView.findViewById(R.id.note);

            if (SinaxApplication.THEME == 1) {
                dateView.setTextColor(Color.BLACK);
                noteView.setTextColor(Color.BLACK);

                itemView.setBackgroundColor(Color.WHITE);
            }
        }
    }
}
