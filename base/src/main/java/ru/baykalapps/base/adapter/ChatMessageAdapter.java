package ru.baykalapps.base.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.db.dao.ChatMessageDao;
import ru.baykalapps.base.model.ChatMessage;
import ru.baykalapps.base.utils.ChatImageBitmapProcessor;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mLayoutInflater;

    private SimpleDateFormat mTimeFormat;
    private SimpleDateFormat mDiverDateFormat;

    private ChatMessageDao chatMessageDao;

    private DisplayImageOptions mDisplayImageOptions;

    private List<ChatMessage> list;

    private String mAdminName;

    private View.OnClickListener mOnAvatarClickListener;

    public ChatMessageAdapter(Context context, List<ChatMessage> list, View.OnClickListener onAvatarClick) {
        this.list = list;
        mOnAvatarClickListener = onAvatarClick;
        mLayoutInflater = LayoutInflater.from(context);

        chatMessageDao = SinaxApplication.getDatabaseDao().getChatMessageDao();

        mTimeFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());
        mDiverDateFormat = new SimpleDateFormat(Constant.DATE_FORMAT.DAY, Locale.getDefault());

        mAdminName = context.getString(R.string.app_name_full);
        int mAvatarSize = (int) context.getResources().getDimension(R.dimen.chat_message_avatar);

        mDisplayImageOptions = new DisplayImageOptions.Builder()
                .preProcessor(new ChatImageBitmapProcessor(mAvatarSize, mAvatarSize))
                .cacheOnDisk(true).build();


    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage chatMessage = getItem(position);
        if (chatMessage.getJustDate())
            return 0;

        if (chatMessage.getAdmin())
            return 1;


        return chatMessage.color == 0 ? 2 : 3;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ChatMessage getItem(int position) {
        return list.get(position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 0) {
            View view = mLayoutInflater.inflate(R.layout.item_chat_date, parent, false);
            return new DateViewHolder(view);
        } else {
            View v = mLayoutInflater.inflate(viewType == 1 ? R.layout.item_chat_message_admin : R.layout.item_chat_message, parent, false);
            return new MessageViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ChatMessage message = getItem(position);

        if (getItemViewType(position) == 0) {
            ((DateViewHolder) holder).dateView.setText(mDiverDateFormat.format(message.getDate()));
        } else {
            MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
            messageViewHolder.dateView.setText(mTimeFormat.format(message.getDate()));

            messageViewHolder.avatarView.setTag(message);
            messageViewHolder.avatarView.setOnClickListener(mOnAvatarClickListener);

            if (message.getAdmin()) {
                ImageLoader.getInstance().displayImage("drawable://" + R.mipmap.ic_launcher, messageViewHolder.avatarView);

                messageViewHolder.usernameView.setText(mAdminName);
            } else {
                messageViewHolder.usernameView.setText(message.getUsername());
                ImageLoader.getInstance().displayImage(message.getAvatar(), messageViewHolder.avatarView, mDisplayImageOptions);

                if (message.color == 0) {
                    messageViewHolder.messageContainer.setBackgroundResource(R.drawable.chat_message_dark);
                }
            }

            messageViewHolder.messageView.setText(message.getText());
        }
    }

    public void setData(List<ChatMessage> data) {
        list = data;
        notifyDataSetChanged();
    }

    public class DateViewHolder extends RecyclerView.ViewHolder {
        TextView dateView;

        public DateViewHolder(View itemView) {
            super(itemView);
            dateView = (TextView) itemView.findViewById(R.id.date);
        }
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView dateView;
        ImageView avatarView;
        TextView messageView;
        TextView usernameView;

        View messageContainer;

        public MessageViewHolder(View itemView) {
            super(itemView);

            dateView = (TextView) itemView.findViewById(R.id.date);
            avatarView = (ImageView) itemView.findViewById(R.id.avatar);

            messageView = (TextView) itemView.findViewById(R.id.message);
            usernameView = (TextView) itemView.findViewById(R.id.username);

            messageContainer = itemView.findViewById(R.id.messageContainer);
        }
    }
}
