package ru.baykalapps.base.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.utils.ConverterUtil;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class ContactsLightAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Contact> mContacts;
    private View.OnClickListener mOnContactClick;

    private final String BASE_URL;

    private int mContactRowBackgroundColor = -1;
    private static int sMinHeight = (int) ConverterUtil.dpToPix(Resources.getSystem(), 54);

    public ContactsLightAdapter(List<Contact> contacts, View.OnClickListener onClickListener) {
        mContacts = new ArrayList<>();

        for (Contact contact : contacts) {
            for (ContactRow contactRow : contact.getRows()) {
                Contact c = new Contact();
                c.setType(contact.getType());
                List<ContactRow> contactRows = new ArrayList<>(1);
                contactRows.add(contactRow);
                c.setRows(contactRows);

                mContacts.add(c);
            }

        }
        mOnContactClick = onClickListener;

        BASE_URL = "assets://contacts/"
                + (SinaxApplication.THEME == 1 ? "light/" : "dark/");

    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_light, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_photo_card, parent, false);
            return new HeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (position > 0) {
            ContactsLightAdapter.ViewHolder holder = (ContactsLightAdapter.ViewHolder) viewHolder;
            Context context = holder.type.getContext();

            Contact contact = mContacts.get(position - 1);

            ImageLoader.getInstance().displayImage(
                    BASE_URL + contact.getType() + ".png",
                    holder.icon
            );

            int contactTitle = context.getResources().getIdentifier("contacts_" + contact.getType(), "string", context.getPackageName());
            String type = context.getString(contactTitle);


            List<ContactRow> contactRows = contact.getRows();
            int n = contactRows.size();
            for (int i = 0; i < n; i++) {
                ContactRow row = contactRows.get(i);
                if (row.getTitle() != null) {
                    type += " - " + row.getTitle();
                }

                holder.getView().setTag(R.string.contact_value, row.getValue());
                holder.getView().setTag(R.string.contact_type, contact.getType());
                holder.getView().setOnClickListener(mOnContactClick);

                holder.contact.setText(row.getValue());

            }

            holder.type.setText(type);
        }
    }

    @Override
    public int getItemCount() {
        return mContacts.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView type;
        private TextView contact;

        private View view;

        public View getView() {
            return view;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;


            icon = (ImageView) itemView.findViewById(R.id.icon);
            type = (TextView) itemView.findViewById(R.id.contact_type);
            contact = (TextView) itemView.findViewById(R.id.contact);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
}
