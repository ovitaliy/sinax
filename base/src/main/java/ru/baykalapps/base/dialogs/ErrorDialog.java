package ru.baykalapps.base.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import ru.baykalapps.base.R;
import ru.baykalapps.base.activities.AuthActivity;

public class ErrorDialog {

    public static void showError(Context context, int errorId) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_error, null);
        TextView errorTextView = (TextView) view.findViewById(R.id.error_message);
        errorTextView.setText(errorId);

        final Dialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .create();

        view.findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.positive_action_button).setVisibility(View.GONE);

        dialog.show();
    }

    public static void showUnauthorizedError(final Context context) {
        new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.registration))
                .setMessage(context.getString(R.string.authorization_required))
                .setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        context.startActivity(new Intent(context, AuthActivity.class));
                    }
                })
                .setNegativeButton(context.getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(true)
                .show();
    }


}
