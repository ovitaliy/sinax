package ru.baykalapps.base.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import ru.baykalapps.base.Constant;

/**
 * Created by ovitali on 18.07.2015.
 * Project is Sinax
 */
public class InAppAvatarSourceDialog extends DialogFragment implements View.OnClickListener {

    private OnImageSelectListener mOnImageSelectListener;

    public void setOnImageSelectListener(OnImageSelectListener onImageSelectListener) {
        this.mOnImageSelectListener = onImageSelectListener;
    }

    @Override
    @Nullable
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = new RecyclerView(getActivity());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setAdapter(new Adapter());

        return recyclerView;
    }

    @Override
    public void onClick(View v) {
        String url = v.getTag().toString();

        mOnImageSelectListener.onImageSelected(url);
    }


    private class Adapter extends RecyclerView.Adapter<ViewHolder> {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ImageView imageView = new ImageView(getActivity());
            imageView.setOnClickListener(InAppAvatarSourceDialog.this);
            return new ViewHolder(imageView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.itemView.setTag(Constant.getLocalAvatarUrl(position));

            ImageLoader.getInstance().displayImage(Constant.getLocalAvatarUrl(position), holder.avatarView);

        }

        @Override
        public int getItemCount() {
            return Constant.IN_APP_AVATARS_COUNT;
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView avatarView;

        public ViewHolder(View itemView) {
            super(itemView);
            avatarView = (ImageView) itemView;
        }
    }

    public interface OnImageSelectListener {
        void onImageSelected(String selectedUrl);
    }
}
