package ru.baykalapps.base.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import ru.baykalapps.base.R;
import ru.baykalapps.base.activities.MainActivity;
import ru.baykalapps.base.fragments.DownloadProgressFragment;

public class ConfirmCacheDialog {

    public static void show(final Context context) {
        /*View view = LayoutInflater.from(context).inflate(R.layout.dialog_confirm_cache, null);

        final Dialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .create();

        view.findViewById(R.id.yes_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((MainActivity) context).startFragment(new DownloadProgressFragment(), true);
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.positive_action_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();*/

        new android.support.v7.app.AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.confirm_download))
                .setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((MainActivity) context).startFragment(new DownloadProgressFragment(), true);
                    }
                })
                .setNegativeButton(context.getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(true)
                .show();
    }
}
