package ru.baykalapps.base.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by ovitali on 27.07.2015.
 * Project is Sinax
 */
public class ShowImageDialog {

    private ShowImageDialog(Context context, int drawableId) {
        ImageView view = new ImageView(context);
        view.setAdjustViewBounds(true);

        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().build();
        ImageLoader.getInstance().displayImage("drawable://" + drawableId, view, displayImageOptions);


        Dialog mDialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .create();

        mDialog.show();
    }

    public static void show(Context context, int drawableId) {
        new ShowImageDialog(context, drawableId);
    }

}
