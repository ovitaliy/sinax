package ru.baykalapps.base.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.prefs.AppPrefs;

/**
 * Created by ovitali on 27.07.2015.
 * Project is Sinax
 */
public class SelectFontSizeDialog {

    private Dialog mDialog;
    RadioGroup mRadioGroup;

    private SelectFontSizeDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_select_text_size, null);

        mRadioGroup = (RadioGroup) view.findViewById(R.id.fontSizeRadioGroup);

        int value = AppPrefs.getTextSize();

        for (int i = 0; i < 3; i++) {
            RadioButton radioButton = (RadioButton) mRadioGroup.getChildAt(i);
            if (Constant.DEFAULT_TEXT_SIZE + i * 2 == value) {
                radioButton.setChecked(true);
                break;
            }
        }


        view.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int value = 0;
                for (int i = 0; i < 3; i++) {
                    RadioButton radioButton = (RadioButton) mRadioGroup.getChildAt(i);
                    if (radioButton.isChecked()) {
                        value = Constant.DEFAULT_TEXT_SIZE + i * 2;
                        break;
                    }
                }

                AppPrefs.setTextSize(value);

                mDialog.dismiss();

            }
        });


        mDialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .create();

        mDialog.show();
    }

    public static void show(Context context) {
        new SelectFontSizeDialog(context);
    }

}
