package ru.baykalapps.base.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import ru.baykalapps.base.R;

public class SelectImageSourceDialog implements View.OnClickListener {

    private Dialog mDialog;
    private View.OnClickListener mClick;

    private SelectImageSourceDialog(Context context, View.OnClickListener click) {
        mClick = click;

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_select_image_source, null);

        view.findViewById(R.id.source_app).setOnClickListener(this);
        view.findViewById(R.id.source_camera).setOnClickListener(this);
        view.findViewById(R.id.source_gallery).setOnClickListener(this);

        mDialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(true)
                .create();

        mDialog.show();
    }


    @Override
    public void onClick(View v) {
        mClick.onClick(v);
        mDialog.dismiss();
    }

    public static void show(Context context, View.OnClickListener click) {
        new SelectImageSourceDialog(context, click);
    }
}
