package ru.baykalapps.base.api.gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.model.ContactsList;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class ContactsDeserializerList implements JsonDeserializer<ContactsList> {
    @Override
    public ContactsList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
            context) throws JsonParseException {
        JsonObject obj = json.getAsJsonObject();

        ContactsList contacts = new ContactsList();
        try {
            JsonArray contactsJsonArray = obj.getAsJsonArray("contacts");

            for (int i = 0; i < contactsJsonArray.size(); i++) {
                obj = (JsonObject) contactsJsonArray.get(i);
                obj = obj.getAsJsonObject("category");

                Contact contact = new Contact();
                List<ContactRow> contactRows = new ArrayList<>();
                contact.setRows(contactRows);

                contact.setType(obj.getAsJsonPrimitive("name").getAsString());

                JsonElement contentJson = obj.get("content");

                if (contentJson.isJsonArray()) {
                    JsonArray contentJsonArray = contentJson.getAsJsonArray();

                    for (JsonElement rowJson : contentJsonArray) {
                        ContactRow contactRow = new ContactRow();
                        contactRow.setValue(rowJson.getAsString());

                        contactRows.add(contactRow);
                    }
                } else if (contentJson.isJsonObject()) {
                    JsonObject contentJsonObject = contentJson.getAsJsonObject();

                    contactRows.addAll(getRowsByLang(contentJsonObject, "ru"));
                    contactRows.addAll(getRowsByLang(contentJsonObject, "en"));
                    contactRows.addAll(getRowsByLang(contentJsonObject, "es"));

                }

                contacts.add(contact);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return contacts;
    }

    private List<ContactRow> getRowsByLang(JsonObject contentJsonObject, String lang) {
        List<ContactRow> rows = new ArrayList<>();
        JsonArray contentJsonArray = contentJsonObject.getAsJsonArray(lang);
        for (JsonElement rowJson : contentJsonArray) {

            JsonArray jsonContactRowArray = rowJson.getAsJsonArray();

            ContactRow contactRow = new ContactRow();
            contactRow.setLang(lang);
            contactRow.setValue(jsonContactRowArray.get(0).getAsString());
            contactRow.setTitle(jsonContactRowArray.get(1).getAsString());

            rows.add(contactRow);
        }
        return rows;
    }

}
