package ru.baykalapps.base.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.api.requests.sets.IChatApi;
import ru.baykalapps.base.model.ChatMessage;
import ru.baykalapps.base.model.ChatMessageList;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public class GetChatMessagesListRequest extends RetrofitSpiceRequest<ChatMessageList, IChatApi> {


    public GetChatMessagesListRequest() {
        super(ChatMessageList.class, IChatApi.class);
    }

    @Override
    public ChatMessageList loadDataFromNetwork() throws Exception {
        ChatMessageList chatMessages = getService().get();

        List<ChatMessage> messageList = chatMessages.getMessages();

        SinaxApplication.getDatabaseDao().getChatMessageDao().deleteAll();
        SinaxApplication.getDatabaseDao().getChatMessageDao().insertInTx(messageList);

        return chatMessages;
    }
}
