package ru.baykalapps.base.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.api.requests.sets.IAuthApi;
import ru.baykalapps.base.model.RegistrationResponse;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class RegistrationRequest extends RetrofitSpiceRequest<RegistrationResponse, IAuthApi> {

    private String mEmail;
    private String mUsername;
    private String mPassword;

    public RegistrationRequest(String email, String username, String password) {
        super(RegistrationResponse.class, IAuthApi.class);
        mEmail = email;
        mUsername = username;
        mPassword = password;
    }

    @Override
    public RegistrationResponse loadDataFromNetwork() throws Exception {
        return getService().register(mEmail, mUsername, mPassword, Constant.API_APP_NAME);
    }

}