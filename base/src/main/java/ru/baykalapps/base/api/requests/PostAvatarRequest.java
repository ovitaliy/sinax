package ru.baykalapps.base.api.requests;

import com.google.gson.Gson;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.io.File;
import java.util.List;

import retrofit.mime.TypedFile;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.api.requests.sets.IAuthApi;
import ru.baykalapps.base.model.ChatMessage;
import ru.baykalapps.base.model.RegistrationResponse;
import ru.baykalapps.base.prefs.AuthPref;

/**
 * Created by ovitali on 18.07.2015.
 * Project is Sinax
 */
public class PostAvatarRequest extends RetrofitSpiceRequest<RegistrationResponse, IAuthApi> {
    private TypedFile file;
    private String email;

    public PostAvatarRequest(File file, String email) {
        super(RegistrationResponse.class, IAuthApi.class);
        this.file = new TypedFile("image/png", file);
        this.email = email;
    }

    @Override
    public RegistrationResponse loadDataFromNetwork() throws Exception {
        String response = getService().uploadAvatar(file, email);
        RegistrationResponse registrationResponse = new Gson().fromJson(response, RegistrationResponse.class);
        String oldAvatar = AuthPref.getAvatar();
        AuthPref.setAvatar(registrationResponse.getAvatar());
        List<ChatMessage> chatMessageList = SinaxApplication.getDatabaseDao().getChatMessageDao().queryRaw("WHERE T.'AVATAR'='" + oldAvatar + "'");
        for (ChatMessage message : chatMessageList) {
            message.setAvatar(registrationResponse.getAvatar());
        }
        SinaxApplication.getDatabaseDao().getChatMessageDao().updateInTx(chatMessageList);

        return registrationResponse;
    }
}