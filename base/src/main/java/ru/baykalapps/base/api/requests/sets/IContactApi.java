package ru.baykalapps.base.api.requests.sets;

import retrofit.http.GET;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.model.ContactsList;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public interface IContactApi {

    @GET("/info/" + Constant.API_APP_NAME + "/")
    ContactsList get();

}
