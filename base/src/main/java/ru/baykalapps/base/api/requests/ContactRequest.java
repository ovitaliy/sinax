package ru.baykalapps.base.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.List;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.api.requests.sets.IContactApi;
import ru.baykalapps.base.db.dao.ContactDao;
import ru.baykalapps.base.db.dao.ContactRowDao;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.model.ContactsList;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class ContactRequest extends RetrofitSpiceRequest<ContactsList, IContactApi> {

    public ContactRequest() {
        super(ContactsList.class, IContactApi.class);
    }

    @Override
    public ContactsList loadDataFromNetwork() throws Exception {
        try {
            ContactsList contacts = getService().get();

            ContactDao contactDao = SinaxApplication.getDatabaseDao().getContactDao();
            ContactRowDao contactRowDao = SinaxApplication.getDatabaseDao().getContactRowDao();
            for (int i = 0; i < contacts.size(); i++) {
                Contact contact = contacts.get(i);

                List<ContactRow> contactRows = contact.getRows();

                long contactId = contactDao.insert(contact);
                for (ContactRow contactRow : contactRows) {
                    contactRow.setContactId(contactId);
                }
                contactRowDao.insertInTx(contactRows);
            }

            return contacts;
        } catch (Exception ex) {
            ex.printStackTrace();

            throw ex;
        }

    }

}