package ru.baykalapps.base.api.requests;

import android.util.Log;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import ru.baykalapps.base.api.requests.sets.IChatApi;
import ru.baykalapps.base.model.BaseResponse;
import ru.baykalapps.base.prefs.AuthPref;

/**
 * Created by ovitali on 19.07.2015.
 * Project is Sinax
 */
public class PostMessageRequest extends RetrofitSpiceRequest<BaseResponse, IChatApi> {

    private String text;

    public PostMessageRequest(String text) {
        super(BaseResponse.class, IChatApi.class);
        this.text = text;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        try {
            BaseResponse response = getService().sendMessage(AuthPref.getEmail(), AuthPref.getAuthToken(), text);
            return response;
        } catch (Exception ex) {
            Log.e("PostMessageRequest", "ex", ex);
        }
        return null;
    }
}
