package ru.baykalapps.base.api.gson;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.baykalapps.base.model.Category;
import ru.baykalapps.base.model.CategoryList;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class CategoryDeserializerList implements JsonDeserializer<CategoryList> {
    @Override
    public CategoryList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
            context) throws JsonParseException {
        JsonObject obj = json.getAsJsonObject();

        CategoryList result = new CategoryList();

        result.addAll(getForLang(obj, "ru"));
        result.addAll(getForLang(obj, "en"));
        result.addAll(getForLang(obj, "es"));

        return result;
    }

    private CategoryList getForLang(JsonObject obj, String lang) {
        if (obj.has(lang)) {
            JsonArray jsonArray = obj.getAsJsonArray(lang);
            CategoryList categories = new Gson().fromJson(jsonArray, CategoryList.class);
            for (Category category : categories){
                category.setLang(lang);
            }
            return categories;
        } else {
            return new CategoryList();
        }
    }

}
