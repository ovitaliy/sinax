package ru.baykalapps.base.api.requests.sets;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import ru.baykalapps.base.model.LoginResponse;
import ru.baykalapps.base.model.RegistrationResponse;

/**
 * Created by ovitali on 13.07.2015.
 * Project is Sinax
 */
public interface IAuthApi {

    @FormUrlEncoded
    @POST("/register/")
    RegistrationResponse register(
            @Field("mail") String email,
            @Field("username") String username,
            @Field("password") String password,
            @Field("appname") String appname
    );

    @FormUrlEncoded
    @POST("/auth/")
    LoginResponse auth(
            @Field("mail") String email,
            @Field("password") String password,
            @Field("appname") String appname
    );

    @Multipart
    @POST("/avatar/{email}/")
    String uploadAvatar(@Part("file") TypedFile file, @Path("email") String email);

}
