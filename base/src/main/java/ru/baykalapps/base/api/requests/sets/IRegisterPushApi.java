package ru.baykalapps.base.api.requests.sets;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.model.BaseResponse;

/**
 * Created by ovitali on 13.07.2015.
 * Project is Sinax
 */
public interface IRegisterPushApi {

    @FormUrlEncoded
    @POST("/device/"+ Constant.API_APP_NAME+"/")
    BaseResponse registerDevice(@Field("uid") String uid, @Field("imei") String imei);

}
