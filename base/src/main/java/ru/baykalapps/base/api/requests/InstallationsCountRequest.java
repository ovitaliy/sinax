package ru.baykalapps.base.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import ru.baykalapps.base.api.requests.sets.IRevisionApi;
import ru.baykalapps.base.model.Installations;
import ru.baykalapps.base.prefs.AppPrefs;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class InstallationsCountRequest extends RetrofitSpiceRequest<Installations, IRevisionApi> {

    public InstallationsCountRequest() {
        super(Installations.class, IRevisionApi.class);
    }

    @Override
    public Installations loadDataFromNetwork() throws Exception {
        Installations installations = getService().getInstallations();
        AppPrefs.setInstallationsCount(installations.getInstall());
        AppPrefs.setUsersCount(installations.getUsers());
        return installations;
    }

}