package ru.baykalapps.base.api.requests.sets;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public interface ICategoryApi {

    @GET("/category/{id}/")
    String getCategoryData(@Path("id") long id);
}
