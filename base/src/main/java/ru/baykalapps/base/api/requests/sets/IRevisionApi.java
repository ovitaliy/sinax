package ru.baykalapps.base.api.requests.sets;

import retrofit.http.GET;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.model.CategoryList;
import ru.baykalapps.base.model.ContactsList;
import ru.baykalapps.base.model.Installations;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public interface IRevisionApi {

    @GET("/rev/" + Constant.API_APP_NAME + "/")
    Integer getRevision();

    @GET("/users/" + Constant.API_APP_NAME + "/")
    Installations getInstallations();

    @GET("/images/" + Constant.API_APP_NAME + "/?size=1242x1200")
    String getImages();

    @GET("/category-list/" + Constant.API_APP_NAME + "/")
    CategoryList getCategories();

    @GET("/info/" + Constant.API_APP_NAME + "/")
    ContactsList getContacts();


}
