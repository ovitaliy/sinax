package ru.baykalapps.base.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.util.Date;

import retrofit.RestAdapter;
import retrofit.client.Request;
import retrofit.client.UrlConnectionClient;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;
import retrofit.mime.MimeUtil;
import retrofit.mime.TypedInput;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.api.gson.CategoryDeserializerList;
import ru.baykalapps.base.api.gson.ContactsDeserializerList;
import ru.baykalapps.base.api.gson.DateDeserializer;
import ru.baykalapps.base.model.CategoryList;
import ru.baykalapps.base.model.ContactsList;

public class ApiSpiceService extends RetrofitGsonSpiceService {

    @Override
    protected String getServerUrl() {
        return Constant.API;
    }

    @Override
    protected Converter createConverter() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ContactsList.class, new ContactsDeserializerList())
                .registerTypeAdapter(CategoryList.class, new CategoryDeserializerList())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

        return new GsonStringConverter(gson);
    }


    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        return super.createRestAdapterBuilder().setClient(new LongTermUrlConnectionClient());
    }

    private class GsonStringConverter extends GsonConverter {

        private Gson mGson;

        public GsonStringConverter(Gson gson) {
            super(gson);
            mGson = gson;
        }

        @Override
        public Object fromBody(TypedInput body, Type type) throws ConversionException {

            String charset = "UTF-8";
            if (body.mimeType() != null) {
                charset = MimeUtil.parseCharset(body.mimeType());
            }
            InputStreamReader isr = null;
            try {
                isr = new InputStreamReader(body.in(), charset);
                if (TypeToken.get(String.class).getType() == (type)) {
                    return IOUtils.toString(body.in());
                }
                return mGson.fromJson(isr, type);
            } catch (IOException e) {
                throw new ConversionException(e);
            } catch (JsonParseException e) {
                try {
                    return IOUtils.toString(body.in());
                } catch (IOException ex) {
                    throw new ConversionException(e);
                }
            } finally {
                if (isr != null) {
                    try {
                        isr.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
    }

    public final class LongTermUrlConnectionClient extends UrlConnectionClient {
        @Override
        protected HttpURLConnection openConnection(Request request) throws IOException {
            HttpURLConnection connection = super.openConnection(request);
            connection.setConnectTimeout(60 * 1000);
            connection.setReadTimeout(60 * 1000);
            return connection;
        }
    }
}
