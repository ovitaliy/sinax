package ru.baykalapps.base.api.requests.sets;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.model.BaseResponse;
import ru.baykalapps.base.model.ChatMessageList;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public interface IChatApi {

    @FormUrlEncoded
    @POST("/send-message/" + Constant.API_APP_NAME + "/")
    BaseResponse sendMessage(@Field("mail") String email, @Field("token") String token, @Field("text") String text);

    @GET("/messages/" + Constant.API_APP_NAME + "/")
    ChatMessageList get();
}
