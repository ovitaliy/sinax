package ru.baykalapps.base.api.requests;

import android.util.Log;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.api.requests.sets.IRevisionApi;
import ru.baykalapps.base.db.dao.ContactDao;
import ru.baykalapps.base.db.dao.ContactRowDao;
import ru.baykalapps.base.model.CardImage;
import ru.baykalapps.base.model.CategoryList;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.model.ContactsList;
import ru.baykalapps.base.prefs.AppPrefs;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class RevisionRequest extends RetrofitSpiceRequest<Boolean, IRevisionApi> {

    public RevisionRequest() {
        super(Boolean.class, IRevisionApi.class);
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {
        int revision = getService().getRevision();

        Log.i("RevisionRequest", "revision " + revision);
        int oldRevision = AppPrefs.getRevision();
        if (oldRevision < revision) {
            SinaxApplication.getDatabaseDao().getContactDao().deleteAll();
            SinaxApplication.getDatabaseDao().getContactRowDao().deleteAll();
            SinaxApplication.getDatabaseDao().getCategoryDao().deleteAll();
            SinaxApplication.getDatabaseDao().getCardImageDao().deleteAll();

            File folder;
            if (SinaxApplication.getInstance() != null && (folder = SinaxApplication.getInstance().getExternalCacheDir()) != null) {
                File[] list = folder.listFiles();
                if (list != null) {
                    for (File f : list) {
                        if (f.isDirectory()) {
                            recursiveDelete(f);
                        }
                    }
                }
            }

            String imagesResponse = getService().getImages();

            JSONObject jsonObject = new JSONObject(imagesResponse);
            JSONArray imagesJsonArray = jsonObject.getJSONArray("images");
            List<CardImage> cardImages = new ArrayList<>(imagesJsonArray.length());
            for (int i = 0; i < imagesJsonArray.length(); i++) {
                String image = imagesJsonArray.getString(i);
                CardImage cardImage = new CardImage();
                cardImage.setPath(image);
                cardImages.add(cardImage);
            }
            SinaxApplication.PHOTO_CARDS = new CardImage[cardImages.size()];
            SinaxApplication.PHOTO_CARDS = cardImages.toArray(SinaxApplication.PHOTO_CARDS);

            SinaxApplication.getDatabaseDao().getCardImageDao().insertInTx(cardImages);

            CategoryList categories = getService().getCategories();
            Log.i("CategoryListRequest", "categories " + categories.size());
            SinaxApplication.getDatabaseDao().getCategoryDao().insertInTx(categories);


            ContactsList contacts = getService().getContacts();

            ContactDao contactDao = SinaxApplication.getDatabaseDao().getContactDao();
            ContactRowDao contactRowDao = SinaxApplication.getDatabaseDao().getContactRowDao();
            for (int i = 0; i < contacts.size(); i++) {
                Contact contact = contacts.get(i);

                List<ContactRow> contactRows = contact.getRows();

                long contactId = contactDao.insert(contact);
                for (ContactRow contactRow : contactRows) {
                    contactRow.setContactId(contactId);
                }
                contactRowDao.insertInTx(contactRows);
            }


            AppPrefs.setRevision(revision);
        }

        return oldRevision < revision;
    }

    void recursiveDelete(File file) {
        if (!file.exists())
            return;
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                recursiveDelete(f);
            }
        }
        file.delete();
    }

}