package ru.baykalapps.base.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.api.requests.sets.IAuthApi;
import ru.baykalapps.base.model.LoginResponse;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class LoginRequest extends RetrofitSpiceRequest<LoginResponse, IAuthApi> {

    private String mEmail;
    private String mPassword;

    public LoginRequest(String email, String password) {
        super(LoginResponse.class, IAuthApi.class);
        mEmail = email;
        mPassword = password;
    }

    @Override
    public LoginResponse loadDataFromNetwork() throws Exception {
        return getService().auth(mEmail, mPassword, Constant.API_APP_NAME);
    }

}