package ru.baykalapps.base.api.responces;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public abstract class SimplerRequestListener<T> implements RequestListener<T> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {
        Log.e("SimplerRequestListner", "exception", spiceException);
    }
}
