package ru.baykalapps.base.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import ru.baykalapps.base.api.requests.sets.IRegisterPushApi;
import ru.baykalapps.base.model.BaseResponse;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class RegisterDeviceRequest extends RetrofitSpiceRequest<BaseResponse, IRegisterPushApi> {

    private String mUid;
    private String mImei;

    public RegisterDeviceRequest(String uid, String imei) {
        super(BaseResponse.class, IRegisterPushApi.class);
        mUid = uid;
        mImei = imei;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        return getService().registerDevice(mUid, mImei);
    }

}