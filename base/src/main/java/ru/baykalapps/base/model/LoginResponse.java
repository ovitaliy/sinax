package ru.baykalapps.base.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 13.07.2015.
 * Project is Sinax
 */
public class LoginResponse extends BaseResponse {

    @SerializedName("auth_token")
    String authToken;

    @SerializedName("auth_admin")
    String authAdmin;

    @SerializedName("auth_status")
    String authStatus;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @SerializedName("auth_name")
    String username;

    @SerializedName("auth_avatar")
    String avatar;

    public String getToken() {
        return authToken;
    }

    public void setToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthAdmin() {
        return authAdmin;
    }

    public void setAuthAdmin(String authAdmin) {
        this.authAdmin = authAdmin;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
