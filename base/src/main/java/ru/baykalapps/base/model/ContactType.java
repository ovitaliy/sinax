package ru.baykalapps.base.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public enum ContactType {
    @SerializedName("phone")
    PHONE,

    @SerializedName("email")
    EMAIL,

    @SerializedName("fb")
    FB,

    @SerializedName("twitter")
    TWITTER,

    @SerializedName("google")
    GOOGLE,

    @SerializedName("youtube")
    YOUTUBE,

    @SerializedName("vimeo")
    VIMEO,

    @SerializedName("site")
    SITE

}
