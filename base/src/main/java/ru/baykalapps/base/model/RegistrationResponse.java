package ru.baykalapps.base.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ovitali on 13.07.2015.
 * Project is Sinax
 */
public class RegistrationResponse extends BaseResponse {

    @SerializedName("auth_token")
    String token;

    public String getAvatar() {
        return avatar;
    }

    String avatar;

    public String getToken() {
        return token;
    }
}
