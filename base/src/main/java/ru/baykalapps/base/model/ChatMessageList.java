package ru.baykalapps.base.model;

import java.util.ArrayList;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public class ChatMessageList {
    ArrayList<ChatMessage> messages;

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }
}
