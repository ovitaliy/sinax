package ru.baykalapps.base.model;

/**
 * Created by ovitali on 08.09.2015.
 */
public class CardImage {

    private Long id;
    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public CardImage() {
    }

    public CardImage(Long id, String path) {

        this.id = id;
        this.path = path;
    }

    public CardImage(Long id) {

        this.id = id;
    }
}
