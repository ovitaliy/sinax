package ru.baykalapps.base.model;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public class BaseResponse {

    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
