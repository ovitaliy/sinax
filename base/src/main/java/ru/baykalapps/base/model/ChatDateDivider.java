package ru.baykalapps.base.model;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public class ChatDateDivider implements IChatItem {

    String date;

    int firstItem;
    int lastItem;

    public int getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(int firstItem) {
        this.firstItem = firstItem;
    }

    public ChatDateDivider(String date) {
        this.date = date;
    }

    public ChatDateDivider() {

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getLastItem() {
        return lastItem;
    }

    public void setLastItem(int lastItem) {
        this.lastItem = lastItem;
    }

    @Override
    public int getType() {
        return 0;
    }
}
