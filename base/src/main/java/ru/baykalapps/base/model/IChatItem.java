package ru.baykalapps.base.model;

/**
 * Created by ovitali on 17.07.2015.
 * Project is Sinax
 */
public interface IChatItem {
    int getType();
}
