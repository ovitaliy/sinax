package ru.baykalapps.base;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by ovitali on 06.07.2015.
 * Project is Sinax
 */
public final class Constant {

    public static int IN_APP_AVATARS_COUNT = 20;

    public static String API = "http://www.cardadmin.ru/api/v1/";
    public static String CATEGORY_URL = "http://www.cardadmin.ru/api/v1/category/%d/";

    public static final String API_APP_NAME = "BELLATOR";

    public static final class CONTACTS {
        public static final String ORDER_APP = "http://bkl77.com/";
        public static final String VIEW_ALL_APPS = "https://play.google.com/store/apps/developer?id=Natalya+Lev";

        public static final String SITE = "http://mcard77.com/";
        public static final String ABOUT_APP = SITE + "about/";
        public static final String INSTRUCTION = SITE + "instructions/";

        public static final String BAYKAL_SITE = "http://bkl77.com/";
    }

    public static final class PARAMS {
        public static final String CATEGORY_ID = "category_id";
        public static final String CATEGORY_NAME = "category_name";
    }

    public static final class LOADER {
        public static final int NOTES = 1;
        public static final int CONTACTS = 2;
        public static final int CATEGORY = 3;
        public static final int CHAT = 4;
    }

    public static final class DATE_FORMAT {
        public static final SimpleDateFormat NOTE = new SimpleDateFormat("dd/MM/yyyy\nHH:mm", Locale.getDefault());
        public static final SimpleDateFormat NOTE_SHARE = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        public static final String DAY = "EEEE, dd MMMM";
    }

    public static String getLocalAvatarUrl(int id) {
        return String.format("assets://avatars/%d.png", id);
    }


    public static final String DEFAULT_FONT = "";

    public static final int DEFAULT_TEXT_SIZE = 16;
}
