package ru.baykalapps.base.gcm;

import android.content.Intent;

/**
 * Created by ovitali on 28.07.2015.
 * Project is Sinax
 */
public class InstanceIDListenerService extends com.google.android.gms.iid.InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
