package ru.baykalapps.base;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.db.dao.DaoMaster;
import ru.baykalapps.base.db.dao.DaoSession;
import ru.baykalapps.base.model.CardImage;
import ru.baykalapps.base.prefs.AppPrefs;
import ru.baykalapps.base.utils.PrefHelper;
import ru.baykalapps.base.utils.UiUtil;

/**
 * Created by ovitali on 06.07.2015.
 * Project is Sinax
 */
public class SinaxApplication extends Application {

    public static int SCREEN_WIDTH = -1;
    public static int SCREEN_HEIGHT = -1;

    public static CardImage PHOTO_CARDS[];

    /**
     * -1 - dark
     * 1 - light
     */
    public static int THEME = 0;

    public static int GREY_COLOR;

    private static SinaxApplication sInstance;

    private static DaoSession sDaoSession;
    public static int VISIT_CARD;
    public static String MAIN_LABEL;
    public static int APP_ICON;

    public static boolean TRANSLATION_ENABLED = false;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;

        EventBus.builder().sendNoSubscriberEvent(false).installDefaultEventBus();

        PrefHelper.init(this);

        getDatabaseDao();

        THEME = AppPrefs.getTheme();

        GREY_COLOR = getResources().getColor(R.color.category_text_color);

        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(displayImageOptions)
                .build();
        ImageLoader.getInstance().init(config);


        String lang = AppPrefs.getLanguage();
        if (lang == null) {
            lang = "ru";
            AppPrefs.setLanguage(lang);
        }

        UiUtil.switchLanguage(this, lang);
    }

    public static SinaxApplication getInstance() {
        return sInstance;
    }

    private static ExecutorService sExecutorService;

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private static ExecutorService getExecutorService() {
        if (sExecutorService == null) {
            sExecutorService = Executors.newFixedThreadPool(1);
        }
        return sExecutorService;
    }

    public static void execute(Runnable task) {
        getExecutorService().submit(task);
    }

    public static DaoSession getDatabaseDao() {
        if (sDaoSession == null) {
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(sInstance, "sinax", null);
            SQLiteDatabase db = helper.getWritableDatabase();
            DaoMaster daoMaster = new DaoMaster(db);

            sDaoSession = daoMaster.newSession();
        }
        return sDaoSession;
    }

    // ----------------


}
