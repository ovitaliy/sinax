package ru.baykalapps.base.db.loaders;

import android.content.Context;

import java.util.List;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.model.Note;

/**
 * Created by ovitali on 18.02.2015.
 */
public class NotesLoader extends BaseLoader<List<Note>> {

    public NotesLoader(Context context) {
        super(context);
    }

    @Override
    public List<Note> loadInBackground() {
        return SinaxApplication.getDatabaseDao().getNoteDao().loadAll();
    }
}
