package ru.baykalapps.base.db.loaders;

import android.content.Context;
import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.db.dao.ChatMessageDao;
import ru.baykalapps.base.model.ChatMessage;

public class ChatMessageLoader extends BaseLoader<List<ChatMessage>> {

    private String mUserName;

    public ChatMessageLoader(Context context, String userName) {
        super(context);
        mUserName = userName;
    }

    @Override
    public List<ChatMessage> loadInBackground() {
        ChatMessageDao chatMessageDao = SinaxApplication.getDatabaseDao().getChatMessageDao();
        ChatMessageDao dao = SinaxApplication.getDatabaseDao().getChatMessageDao();

        String where = null;
        if (mUserName != null) {
            if (!mUserName.equals("admin"))
                where = ChatMessageDao.Properties.Username.name + "='" + mUserName + "'";
            else
                where = ChatMessageDao.Properties.Admin.name + "='1'";
        }

        Cursor cursor = SinaxApplication.getDatabaseDao().getDatabase().query(
                chatMessageDao.getTablename(),
                chatMessageDao.getAllColumns(),
                where, null, null, null, "_id ASC");

        List<ChatMessage> list = new ArrayList<>(cursor.getCount());

        final int backgroundColor = 10282989;

        ChatMessage chatMessage;

        int lastColor = backgroundColor;
        String lastUser = "";
        if (cursor.moveToFirst()) {
            chatMessage = dao.readEntity(cursor, 0);
            lastUser = chatMessage.getUsername();
            do {
                chatMessage = dao.readEntity(cursor, 0);
                list.add(chatMessage);

                if (chatMessage.getAdmin())
                    continue;
                if (chatMessage.getJustDate() != null && chatMessage.getJustDate())
                    continue;


                if (!chatMessage.getUsername().equals(lastUser)) {
                    lastColor = lastColor == 0 ? backgroundColor : 0;
                }
                lastUser = chatMessage.getUsername();
                chatMessage.color = lastColor;

            } while (cursor.moveToNext());
        }
        return getChatMessages(list);

        //return SinaxApplication.getDatabaseDao().getChatMessageDao().loadAll();
    }

    private List<ChatMessage> getChatMessages(List<ChatMessage> messageList) {
        List<ChatMessage> chatItems = new ArrayList<>();

        String lastDate = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM", Locale.getDefault());

        int n = messageList.size();

        for (int i = 0; i < n; i++) {
            ChatMessage chatMessage = messageList.get(i);

            String date = dateFormat.format(chatMessage.getDate());

            if (!date.equals(lastDate)) {
                ChatMessage dateMessage = new ChatMessage();
                dateMessage.setJustDate(true);
                dateMessage.setDate(chatMessage.getDate());
                chatItems.add(dateMessage);
                lastDate = date;
            }

            chatMessage.setJustDate(false);
            chatItems.add(chatMessage);
        }

        n = chatItems.size();
        for (int i = n - 1; i >= 0; i--) {
            ChatMessage chatMessage = chatItems.get(i);
            chatMessage.setId((long) (n - i));
        }

        return chatItems;
    }
}
