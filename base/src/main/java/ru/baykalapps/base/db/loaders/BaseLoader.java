package ru.baykalapps.base.db.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {

    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }


}
