package ru.baykalapps.base.db.callbacks;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

public abstract class SimpleLoaderCallbacks<T> implements LoaderManager.LoaderCallbacks<T> {
    @Override
    public void onLoaderReset(Loader loader) {

    }
}
