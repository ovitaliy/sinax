package ru.baykalapps.base.db.loaders;

import android.content.Context;

import java.util.List;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.db.dao.CategoryDao;
import ru.baykalapps.base.model.Category;
import ru.baykalapps.base.prefs.AppPrefs;

/**
 * Created by ovitali on 14.07.2015.
 * Project is Sinax
 */
public class CategoryLoader extends BaseLoader<List<Category>> {

    public CategoryLoader(Context context) {
        super(context);
    }

    @Override
    public List<Category> loadInBackground() {
        CategoryDao categoryDao = SinaxApplication.getDatabaseDao().getCategoryDao();

        String lang = AppPrefs.getLanguage();

        return categoryDao.queryBuilder().where(CategoryDao.Properties.LANG.eq(lang)).list();
    }


}
