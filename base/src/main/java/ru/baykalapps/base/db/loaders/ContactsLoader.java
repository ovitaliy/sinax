package ru.baykalapps.base.db.loaders;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.prefs.AppPrefs;

/**
 * Created by ovitali on 18.02.2015.
 */
public class ContactsLoader extends BaseLoader<List<Contact>> {

    public ContactsLoader(Context context) {
        super(context);
    }

    @Override
    public List<Contact> loadInBackground() {
        String lang = AppPrefs.getLanguage();
        List<Contact> contactList = SinaxApplication.getDatabaseDao().getContactDao().loadAll();
        List<Contact> filteredContactList = new ArrayList<>();
        for (Contact c : contactList) {
            List<ContactRow> rows = c.getRows();
            List<ContactRow> filteredRows = new ArrayList<>();

            for (ContactRow contactRow : rows) {
                if (contactRow.getLang() == null || contactRow.getLang().equals(lang)) {
                    filteredRows.add(contactRow);
                }
            }

            if (filteredRows.size() > 0) {
                c.setRows(filteredRows);
                filteredContactList.add(c);
            }
        }

        return filteredContactList;
    }
}
