package ru.baykalapps.base.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by ovitali on 21.07.2015.
 * Project is Sinax
 */
public class InternetConnectionStatus {
    public static boolean isConnected(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        return !(i == null || (!i.isConnected() || !i.isAvailable()) || (!i.isConnectedOrConnecting()));
    }
}
