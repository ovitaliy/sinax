package ru.baykalapps.base.utils.downloads;

import java.io.File;
import java.io.PrintWriter;

/**
 * Created by ovitali on 21.07.2015.
 * Project is Sinax
 */
public class SaveHtmlRunnable implements Runnable {

    String html;
    File file;

    public SaveHtmlRunnable(String html, File file) {
        this.html = html;
        this.file = file;
    }

    @Override
    public void run() {
        try {
            PrintWriter printWriter = new PrintWriter(file);
            printWriter.write(html);
            printWriter.flush();
            printWriter.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
