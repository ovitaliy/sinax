package ru.baykalapps.base.utils;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.process.BitmapProcessor;

/**
 * Created by ovitali on 10.09.2015.
 */
public class ChatImageBitmapProcessor implements BitmapProcessor {

    int width;
    int height;


    public ChatImageBitmapProcessor(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public Bitmap process(Bitmap bitmap) {
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }
}
