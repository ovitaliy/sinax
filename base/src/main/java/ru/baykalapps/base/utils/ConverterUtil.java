package ru.baykalapps.base.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ovitali on 07.04.2015.
 * Project is Magazines4Free
 */
public class ConverterUtil {

    public static float dpToPix(Context context, float value) {
        return dpToPix(context.getResources(), value);
    }

    public static float dpToPix(Resources resources, float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.getDisplayMetrics());
    }

    public static float pixToDp(Context context, float value) {
        return pixToDp(context.getResources(), value);
    }

    public static float pixToDp(Resources resources, float value) {
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return value / (metrics.densityDpi / 160f);
    }

    public static String getBase64FromImageAssets(Context context, String image) {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(image);

            String encodedImage = "";
            byte[] b;

            b = IOUtils.toByteArray(inputStream);
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            encodedImage = encodedImage.replaceAll(" ", "");
            encodedImage = encodedImage.replaceAll("\t", "");
            encodedImage = encodedImage.replaceAll("\n", "");
            encodedImage = encodedImage.replaceAll("\r", "");
            return encodedImage;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("");
        }
    }

}
