package ru.baykalapps.base.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.WeakHashMap;

/**
 * Created by michael on 10.12.14.
 */
public class TypefaceCache {

    private static WeakHashMap<String, Typeface> sCache;

    public static Typeface getTypeface(Context context, String typefaceName) {
        if (sCache == null) {
            sCache = new WeakHashMap<String, Typeface>(5);
        }
        Typeface tf = sCache.get(typefaceName);
        if (tf == null) {
            tf = Typeface.createFromAsset(context.getAssets(),
                    String.format("fonts/%s.ttf", typefaceName));
            sCache.put(typefaceName, tf);
        }
        return tf;
    }

}
