package ru.baykalapps.base.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ovitali on 23.12.2014.
 */
public class PrefHelper {

    private static SharedPreferences sSharedPreferences;

    public static void init(Context context) {
        String name = context.getPackageName();
        sSharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static SharedPreferences getPreferences() {
        return sSharedPreferences;
    }

    public static String getStringPref(String key) {
        return getStringPref(key, null);
    }

    public static String getStringPref(String key, String def) {
        return sSharedPreferences.getString(key, def);
    }

    public static void setStringPref(String key, String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void removeStringPref(String key) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public static int getIntPref(String key) {
        return sSharedPreferences.getInt(key, -1);
    }

    public static int getIntPref(String key, int def) {
        return sSharedPreferences.getInt(key, def);
    }

    public static void setIntPref(String key, int value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static boolean getBooleanPref(String key) {
        return sSharedPreferences.getBoolean(key, false);
    }

    public static void setBooleanPref(String key, boolean value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
}
