package ru.baykalapps.base.utils.downloads;

import android.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ovitali on 21.07.2015.
 * Project is Sinax
 */
public class SaveYoutubeRunnable implements Runnable {

    String url;
    File file;

    public SaveYoutubeRunnable(String url, File file) {
        this.url = url;
        this.file = file;
    }

    @Override
    public void run() {
        try {
            if (file != null && file.exists()) {
                return;
            }

            int andIdx = url.indexOf('&');
            if (andIdx >= 0) {
                url = url.substring(0, andIdx);
            }

            String videoId;


            ;
            if (!url.contains("v=")) {
                videoId = url.substring(url.lastIndexOf("/") + 1);
                url = "http://www.youtube.com/watch?v=" + videoId;
            } else {
                videoId = url.substring(url.indexOf("?v=") + 3);

            }
            String thumbUrl = "https://img.youtube.com/vi/" + videoId + "/hqdefault.jpg";

            List<Video> videos = getStreamingUrisFromYouTubePage(url);
            for (Video v : videos) {
                try {

                    HttpURLConnection conn;

                    conn = (HttpURLConnection) new URL(v.url).openConnection();
                    File videoFile = new File(file.getAbsolutePath() + "." + v.ext);
                    IOUtils.copyLarge(conn.getInputStream(), new FileOutputStream(videoFile));

                    File thumbFile = new File(file.getAbsolutePath() + ".jpg");
                    if (!thumbFile.exists()) {
                        conn = (HttpURLConnection) new URL(thumbUrl).openConnection();
                        IOUtils.copyLarge(conn.getInputStream(), new FileOutputStream(thumbFile));
                    }

                    break;
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static class Meta {
        public String num;
        public String type;
        public String ext;

        Meta(String num, String ext, String type) {
            this.num = num;
            this.ext = ext;
            this.type = type;
        }
    }

    static class Video {
        public String ext = "";
        public String type = "";
        public String url = "";

        Video(String ext, String type, String url) {
            this.ext = ext;
            this.type = type;
            this.url = url;
        }
    }

    public static ArrayList<Video> getStreamingUrisFromYouTubePage(String ytUrl)
            throws IOException {
        if (ytUrl == null) {
            return null;
        }
        //ytUrl = "http://www.youtube.com/watch?v=wgEBpT3F4sY";

        // Remove any query params in query string after the watch?v=<vid> in
        // e.g.

        HttpURLConnection conn = (HttpURLConnection) new URL(ytUrl).openConnection();
        // Get the HTML response
        String userAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";

        String html = IOUtils.toString(conn.getInputStream());
        html = html.replaceAll(" ", "");

        // Parse the HTML response and extract the streaming URIs
        if (html.contains("verify-age-thumb")) {
            Log.w("you",
                    "YouTube is asking for age verification. We can't handle that sorry.");
            return null;
        }

        if (html.contains("das_captcha")) {
            Log.w("you", "Captcha found, please try with different IP address.");
            return null;
        }

  /*      int index = html.indexOf("url_encoded_fmt_stream_map");
        html = html.substring(index + "url_encoded_fmt_stream_map".length() + 4);
        html = html.substring(0, html.indexOf("\""));*/

        Pattern p = Pattern.compile("url_encoded_fmt_stream_map\\\\\":\\\\\"(.*?)\"");
        // Pattern p = Pattern.compile("/stream_map=(.[^&]*?)\"/");
        Matcher m = p.matcher(html);
        List<String> matches = new ArrayList<>();
        while (m.find()) {
            matches.add(m.group());
        }

        if (matches.size() != 1) {
            Log.w("you", "Found zero or too many stream maps.");
            return null;
        }

        String urls[] = matches.get(0).split(",");
        HashMap<String, String> foundArray = new HashMap<>();
        for (String ppUrl : urls) {
            String url = URLDecoder.decode(ppUrl, "UTF-8");

            Pattern p1 = Pattern.compile("itag=([0-9]+?)[&]");
            Matcher m1 = p1.matcher(url);
            String itag = null;
            if (m1.find()) {
                itag = m1.group(1);
            }

            Pattern p2 = Pattern.compile("signature=(.*?)[&]");
            Matcher m2 = p2.matcher(url);
            String sig = null;
            if (m2.find()) {
                sig = m2.group(1);
            }
            if (sig == null) {
                p2 = Pattern.compile("sig=(.*?)[&]");
                m2 = p2.matcher(url);
                if (m2.find()) {
                    sig = m2.group(1);
                }
            }

            int unIndex = url.indexOf("url=");
            String um = null;
            if (unIndex > 0) {
                um = url.substring(unIndex + 4);
                if (um.contains("\\")) {
                    um = um.substring(0, um.indexOf("\\"));
                }
            }

            if (itag != null && sig != null && um != null) {
                foundArray.put(itag, URLDecoder.decode(um, "UTF-8") + "&"
                        + "signature=" + sig);
            }
        }
        if (foundArray.size() == 0) {
            Log.w("you", "Couldn't find any URLs and corresponding signatures");
            return null;
        }

        HashMap<String, Meta> typeMap = new HashMap<>();
        typeMap.put("13", new Meta("13", "3GP", "Low Quality - 176x144"));
        typeMap.put("17", new Meta("17", "3GP", "Medium Quality - 176x144"));
        typeMap.put("36", new Meta("36", "3GP", "High Quality - 320x240"));
        typeMap.put("5", new Meta("5", "FLV", "Low Quality - 400x226"));
        typeMap.put("6", new Meta("6", "FLV", "Medium Quality - 640x360"));
        typeMap.put("34", new Meta("34", "FLV", "Medium Quality - 640x360"));
        typeMap.put("35", new Meta("35", "FLV", "High Quality - 854x480"));
        typeMap.put("43", new Meta("43", "WEBM", "Low Quality - 640x360"));
        typeMap.put("44", new Meta("44", "WEBM", "Medium Quality - 854x480"));
        typeMap.put("45", new Meta("45", "WEBM", "High Quality - 1280x720"));
        typeMap.put("18", new Meta("18", "MP4", "Medium Quality - 480x360"));
        typeMap.put("22", new Meta("22", "MP4", "High Quality - 1280x720"));
        typeMap.put("37", new Meta("37", "MP4", "High Quality - 1920x1080"));
        typeMap.put("33", new Meta("38", "MP4", "High Quality - 4096x230"));

        ArrayList<Video> videos = new ArrayList<>();

        for (String format : typeMap.keySet()) {
            Meta meta = typeMap.get(format);

            if (foundArray.containsKey(format)) {
                Video newVideo = new Video(meta.ext, meta.type,
                        foundArray.get(format));
                videos.add(newVideo);
                Log.d("you", "YouTube Video streaming details: ext:"
                        + newVideo.ext + ", type:" + newVideo.type + ", url:"
                        + newVideo.url);
            }
        }

        return videos;
    }
}
