package ru.baykalapps.base.utils.downloads;

import android.content.Context;
import android.util.Log;

import java.io.File;

/**
 * Created by ovitali on 23.07.2015.
 * Project is Sinax
 */
public class CacheFile {
    long id;

    File mCacheDir;

    public CacheFile(Context context, long id) {
        this.id = id;
        mCacheDir = new File(context.getExternalCacheDir(), String.valueOf(id));
        if (!mCacheDir.exists() && !mCacheDir.mkdirs()) {
            //throw new RuntimeException("cached folder cannot be created");
            //TODO ignore
            Log.e("CacheFile", "folder can't be created " + mCacheDir.getAbsolutePath());
        }
    }

    public File getFolder() {
        return mCacheDir;
    }

    public File getHtmlCacheFile() {
        return new File(mCacheDir, "data.html");
    }

    public File getImageFile(String url) {
        return new File(mCacheDir, getFileName(url));
    }

    public File getYoutubeFile(String url) {
        String fileName = getFileName(url);
        for (File f : mCacheDir.listFiles()) {
            String path = f.getAbsolutePath();
            if (path.contains(fileName) && !path.endsWith(".jpg")) {
                return f;
            }
        }

        return new File(mCacheDir, fileName);
    }

    public File getYoutubeThumbFile(String url) {
        String fileName = getFileName(url);
        int index = fileName.indexOf(".");
        if (index > 0)
            fileName = fileName.substring(0, index);
        fileName += ".jpg";
        return new File(mCacheDir, fileName);
    }

    public String getFileName(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

}
