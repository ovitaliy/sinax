package ru.baykalapps.base.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by ovitali on 10.04.2015.
 * Project is Magazines4Free
 */
public class UiUtil {

    public static void setTextValue(View v, int viewId, int value) {
        String sValue = value > 0 ? String.valueOf(value) : "";
        setTextValue(v, viewId, sValue);
    }

    public static void setTextValue(View v, int viewId, String value) {
        TextView view = (TextView) v.findViewById(viewId);
        view.setText(value);
    }

    public static String getTextValue(View v, int viewId) {
        TextView view = (TextView) v.findViewById(viewId);
        return view.getText().toString().trim();
    }

    public static int getIntValue(View v, int viewId) {
        try {
            return Integer.parseInt(getTextValue(v, viewId));
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void removeMeFromParent(View view) {
        ViewGroup parent = (ViewGroup) view.getParent();
        parent.removeView(view);
    }

    public static void hideKeyboard(EditText target) {
        InputMethodManager imm = (InputMethodManager) target.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
    }

    public static void setBackgroundDrawable(View v, Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackgroundDrawable(drawable);
        } else {
            v.setBackground(drawable);
        }
    }

    public static void openLink(Context context, String link) {
        try {
            Uri uri = Uri.parse(link);
            context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void share(Context context, String title, String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(shareIntent, title));
    }

    public static void switchLanguage(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
