package ru.baykalapps.base.utils.downloads;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ovitali on 21.07.2015.
 * Project is Sinax
 */
public class SaveImageRunnable implements Runnable {

    String url;
    File file;

    public SaveImageRunnable(String url, File file) {
        this.url = url;
        this.file = file;
    }

    @Override
    public void run() {
        try {
            if (file.exists()) {
                return;
            }

            URL url = new URL(this.url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            IOUtils.copyLarge(conn.getInputStream(), new FileOutputStream(file));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
