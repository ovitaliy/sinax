package ru.baykalapps.base.events;

/**
 * Created by ovitali on 23.07.2015.
 * Project is Sinax
 */
public class CacheProgressEvent {

    private int progress;

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public CacheProgressEvent(int progress) {
        this.progress = progress;
    }
    public CacheProgressEvent(float progress) {
        this.progress = (int) progress;
    }
}
