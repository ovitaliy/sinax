package ru.baykalapps.base.events;

/**
 * Created by ovitali on 09.07.2015.
 * Project is Sinax
 */
public final class NoteEvent {

    public enum EventType {
        DELETE, RELOAD
    }

    private EventType mEventType = EventType.RELOAD;
    private long mId;

    public EventType getEventType() {
        return mEventType;
    }

    public final long getId() {
        return mId;
    }

    public NoteEvent() {

    }

    public NoteEvent(EventType eventType) {
        mEventType = eventType;
    }
    public NoteEvent(EventType eventType, long id) {
        mEventType = eventType;
        mId = id;
    }


}
