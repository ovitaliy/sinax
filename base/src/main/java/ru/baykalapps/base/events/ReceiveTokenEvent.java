package ru.baykalapps.base.events;

/**
 * Created by ovitali on 29.07.2015.
 * Project is Sinax
 */
public class ReceiveTokenEvent {
    String token;

    public ReceiveTokenEvent(String token) {
        this.token = token;
    }

    public String getToken() {

        return token;
    }
}
