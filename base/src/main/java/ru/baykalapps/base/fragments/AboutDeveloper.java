package ru.baykalapps.base.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.utils.UiUtil;

/**
 * Created by ovitali on 09.07.2015.
 * Project is Sinax
 */
public class AboutDeveloper extends BaseMenuFragment implements View.OnClickListener {

    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        getActivity().setTitle(R.string.developer);
        menu.clear();
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_developers, container, false);
        v.findViewById(R.id.to_order_app).setOnClickListener(this);
        v.findViewById(R.id.view_all_apps).setOnClickListener(this);
        v.findViewById(R.id.footer).setOnClickListener(this);
        v.findViewById(R.id.open_site).setOnClickListener(this);
        v.findViewById(R.id.open_site_2).setOnClickListener(this);
        v.findViewById(R.id.open_email).setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.to_order_app) {
            UiUtil.openLink(getActivity(), Constant.CONTACTS.ORDER_APP);

        } else if (i == R.id.view_all_apps) {
            UiUtil.openLink(getActivity(), Constant.CONTACTS.VIEW_ALL_APPS);

        } else if (i == R.id.open_site || i == R.id.footer || i == R.id.open_site_2) {
            UiUtil.openLink(getActivity(), Constant.CONTACTS.BAYKAL_SITE);

        } else if (i == R.id.open_email) {
            UiUtil.openLink(getActivity(), "mailto:" + ((TextView) v).getText().toString());

        }
    }
}
