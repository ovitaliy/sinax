package ru.baykalapps.base.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.fragments.base.BaseMenuSpiceFragment;
import ru.baykalapps.base.prefs.AppPrefs;
import ru.baykalapps.base.utils.InternetConnectionStatus;
import ru.baykalapps.base.utils.UiUtil;
import ru.baykalapps.base.utils.downloads.CacheFile;
import ru.baykalapps.base.utils.downloads.SaveHtmlRunnable;
import ru.baykalapps.base.utils.downloads.SaveImageRunnable;

/**
 * Created by ovitali on 14.07.2015.
 * Project is Sinax
 */
public class ViewCategoryFragment extends BaseMenuSpiceFragment {

    public static ViewCategoryFragment newInstance(long categoryId, String categoryName) {
        ViewCategoryFragment viewCategoryFragment = new ViewCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constant.PARAMS.CATEGORY_ID, categoryId);
        bundle.putString(Constant.PARAMS.CATEGORY_NAME, categoryName);
        viewCategoryFragment.setArguments(bundle);

        return viewCategoryFragment;
    }

    private CacheFile mCacheFile;

    WebView mWebView;

    private String mCategoryUrl;

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_category, container, false);

        mCacheFile = new CacheFile(getActivity(), getArguments().getLong(Constant.PARAMS.CATEGORY_ID));

        mWebView = (WebView) v;

        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

        mWebView.setWebViewClient(new CacheWebViewClient());
        mWebView.setWebChromeClient(new CacheWebChromeClient());

        if (InternetConnectionStatus.isConnected(getActivity())) {
            mWebView.addJavascriptInterface(new OnlineJavaScriptInterface(), "HTMLOUT");

            if (mCacheFile.getHtmlCacheFile().exists())
                mCategoryUrl = "file://" + mCacheFile.getHtmlCacheFile().getAbsolutePath();
            else
                mCategoryUrl = String.format(Constant.CATEGORY_URL, getArguments().getLong(Constant.PARAMS.CATEGORY_ID));

            mWebView.loadUrl(mCategoryUrl);
        } else {
            mWebView.addJavascriptInterface(new OfflineJavaScriptInterface(), "HTMLOUT");

            mCategoryUrl = "file://" + mCacheFile.getHtmlCacheFile().getAbsolutePath();
            mWebView.loadUrl(mCategoryUrl);
        }
        return v;
    }

    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().setTitle(getArguments().getString(Constant.PARAMS.CATEGORY_NAME));
        inflater.inflate(R.menu.view_notes, menu);
        if (SinaxApplication.THEME == 1) {
            menu.findItem(R.id.toolbar_share).getIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
        } else {
            menu.findItem(R.id.toolbar_share).getIcon().setColorFilter(null);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.toolbar_share) {
            share();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void share() {
        Context context = getActivity();
        String title = context.getString(R.string.share);

        String text = getArguments().getString(Constant.PARAMS.CATEGORY_NAME) + " - "
                + String.format(Constant.CATEGORY_URL, getArguments().getLong(Constant.PARAMS.CATEGORY_ID));

        UiUtil.share(context, title, text);
    }

    private class OnlineJavaScriptInterface {
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(String html) {
            if (!mCacheFile.getHtmlCacheFile().exists())
                SinaxApplication.execute(new SaveHtmlRunnable(html, mCacheFile.getHtmlCacheFile()));
        }

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processImages(String data) {
            if (TextUtils.isEmpty(data)) return;
            String[] images = data.split(",");
            for (String image : images) {
                if (!mCacheFile.getImageFile(image).exists())
                    SinaxApplication.execute(new SaveImageRunnable(image, mCacheFile.getImageFile(image)));
            }
        }

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processYoutube(String data) throws IOException {
            if (TextUtils.isEmpty(data)) return;
            String[] youtubes = data.split(",");
            for (String youtube : youtubes) {
               /* if (!mCacheFile.getYoutubeFile(youtube).exists())
                    SinaxApplication.execute(new SaveYoutubeRunnable(youtube, mCacheFile.getYoutubeFile(youtube)));*/

                File thumb = mCacheFile.getYoutubeThumbFile(youtube);
                boolean thumbExists = thumb.exists();
                String thumbFile = thumbExists
                        ? mCacheFile.getYoutubeThumbFile(youtube).getAbsolutePath()
                        : "https://img.youtube.com/vi/" + getVideoId(youtube) + "/hqdefault.jpg";
                if (!thumbExists) {
                    SinaxApplication.execute(new SaveImageRunnable(thumbFile, thumb.getAbsoluteFile()));
                }

                String inject = IOUtils.toString(getActivity().getAssets().open("inject.js"));

                inject = inject.replace("{thumbUrl}", thumbFile);
                inject = inject.replace("{playLink}", youtube);

                final String injection = inject;

                if (isVisible()) {
                    mWebView.post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:" + injection);
                        }
                    });
                }
            }
        }

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processVideoRedirect(String link) {
            File localVideoFile = mCacheFile.getYoutubeFile(link);
            if (localVideoFile.exists()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(localVideoFile.getAbsolutePath()), "video/mp4");
                startActivity(intent);
            } else {
                UiUtil.openLink(getActivity(), link);
            }
        }


    }

    private class OfflineJavaScriptInterface {
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(final String html) {
            if (getView() == null)
                return;

            Pattern imagePattern = Pattern.compile("<iframe(.*?)>");
            Matcher imageMatcher = imagePattern.matcher(html);


            while (imageMatcher.find()) {
                String iframeTag = imageMatcher.group();

                String youtubeVideoUrl = getSrcFromTag(iframeTag);

                File thumbFile = mCacheFile.getYoutubeThumbFile(getSrcFromTag(iframeTag));
                File videoFile = mCacheFile.getYoutubeFile(youtubeVideoUrl);

                if (!videoFile.exists() || !thumbFile.exists())
                    continue;

                try {
                    String inject = IOUtils.toString(getActivity().getAssets().open("inject.js"));

                    inject = inject.replace("{thumbUrl}", thumbFile.getAbsolutePath());
                    inject = inject.replace("{playLink}", videoFile.getAbsolutePath());

                    final String injection = inject;


                    getView().post(new Runnable() {
                        @Override
                        public void run() {
                            mWebView.loadUrl("javascript:" + injection);
                        }
                    });


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }


        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processVideoRedirect(String link) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(link), "video/mp4");
            startActivity(intent);
        }
    }


    private void applyFontSize(WebView view) {
        int textSize = AppPrefs.getTextSize();
        view.loadUrl("javascript: jQuery(\"*\").css(\"font-size\",\""
                + textSize + "px\")");
    }

    private class CacheWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            view.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");

            view.loadUrl("javascript:window.HTMLOUT.processImages($('img').map(function(){\n" +
                    "    return $(this).attr('src')\n" +
                    "}).get().join(','));");

            view.loadUrl("javascript:$('#content').css({'width': '100%'});");
            // view.loadUrl("javascript:$('iframe').on('click', );");
            view.loadUrl("javascript:window.HTMLOUT.processYoutube($('.youtube').map(function(){\n" +
                    "    return $(this).attr('src')\n" +
                    "}).get().join(','));");

            applyFontSize(view);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!url.equals(mCategoryUrl)) {
                UiUtil.openLink(getActivity(), url);
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            WebResourceResponse webResourceResponse = null;
            File f;
            try {
                if (url.contains("jquery")) {
                    String assetPath = "js/jquery.js";
                    webResourceResponse = new WebResourceResponse("text/javascript", "UTF-8", getActivity().getAssets().open(assetPath));
                } else if (
                        (url.endsWith("png") || url.endsWith("jpg"))
                                && (f = mCacheFile.getImageFile(url)) != null
                                && f.exists()) {
                    webResourceResponse = new WebResourceResponse("image/*", "UTF-8", new FileInputStream(f));
                } else {
                    webResourceResponse = super.shouldInterceptRequest(view, url);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return webResourceResponse;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String
                failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }
    }

    private class CacheWebChromeClient extends WebChromeClient {
        @Override
        public boolean onConsoleMessage(@NonNull ConsoleMessage consoleMessage) {
            return super.onConsoleMessage(consoleMessage);
        }
    }

    private String getSrcFromTag(String tag) {
        Matcher srcMatcher = Pattern.compile("src=\"(.*?)\"").matcher(tag);

        if (srcMatcher.find()) {
            String url = srcMatcher.group();

            url = url.replaceAll("\"", "");
            url = url.replace("src=", "");

            return url;
        }
        return null;
    }

    private String getVideoId(String src) {

        String videoId;
        if (src.contains("?v=")) {
            videoId = src.substring(src.indexOf("?v=") + 3);
        } else {
            videoId = src.substring(src.lastIndexOf("/") + 1);
        }
        return videoId;

    }


}
