package ru.baykalapps.base.fragments;


import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.events.CacheProgressEvent;
import ru.baykalapps.base.services.CacheService;

public class DownloadProgressFragment extends Fragment {


    public DownloadProgressFragment() {
    }


    private ProgressBar mProgressBar;
    private TextView mProgressText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_download_progress, container, false);

        mProgressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        mProgressBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        mProgressText = (TextView) v.findViewById(R.id.progressText);

        if (SinaxApplication.THEME == 1) {
            mProgressText.setTextColor(SinaxApplication.GREY_COLOR);
        }

        CacheService.startActionCache(getActivity());

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(CacheProgressEvent event) {
        mProgressBar.setProgress(event.getProgress());
        mProgressText.setText(event.getProgress() + "%");

        if (event.getProgress() == 100) {
            getActivity().onBackPressed();
        }
    }
}
