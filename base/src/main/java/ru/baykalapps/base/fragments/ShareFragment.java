package ru.baykalapps.base.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.dialogs.ShowImageDialog;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.utils.UiUtil;

/**
 * Created by ovitali on 09.07.2015.
 * Project is Sinax
 */
public class ShareFragment extends BaseMenuFragment implements View.OnClickListener {
    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().setTitle(R.string.share);
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_share, container, false);
        view.findViewById(R.id.share_email).setOnClickListener(this);
        view.findViewById(R.id.share_qr).setOnClickListener(this);
        view.findViewById(R.id.share_copy).setOnClickListener(this);
        view.findViewById(R.id.share_sms).setOnClickListener(this);
        view.findViewById(R.id.share_visit).setOnClickListener(this);
        view.findViewById(R.id.share).setOnClickListener(this);

        TextView mCardView = ((TextView) view.findViewById(R.id.mcard));
        mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtil.openLink(getActivity(), Constant.CONTACTS.SITE);
            }
        });
        mCardView.setTextColor(
                SinaxApplication.THEME == -1
                        ? getResources().getColor(R.color.main_button_pressed)
                        : getResources().getColor(R.color.mcard_light));

        return view;
    }

    @Override
    public void onClick(View v) {

        int i = v.getId();
        if (i == R.id.share_email) {
            shareViaEmail();

        } else if (i == R.id.share_sms) {
            shareViaSms();

        } else if (i == R.id.share_qr) {
            ShowImageDialog.show(getActivity(), R.drawable.qr_code);

        } else if (i == R.id.share_visit) {
            ShowImageDialog.show(getActivity(), SinaxApplication.VISIT_CARD);

        } else if (i == R.id.share_copy) {
            copyToClipboard();

        } else if (i == R.id.share) {
            share();

        }
    }

    private void share() {
        Context context = getActivity();
        String title = context.getString(R.string.share);

        String text = context.getString(R.string.app_name_full) + ": " + getLinkToApp();

        UiUtil.share(context, title, text);
    }

    private void shareViaEmail() {
        Intent shareIntent = new Intent(Intent.ACTION_SENDTO);
        shareIntent.setData(Uri.parse("mailto:"));
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name_full));
        shareIntent.putExtra(Intent.EXTRA_TEXT, getLinkToApp());
        getActivity().startActivity(Intent.createChooser(shareIntent, getString(R.string.share)));
    }

    private void shareViaSms() {
        Intent shareIntent = new Intent(Intent.ACTION_SENDTO);
        shareIntent.setData(Uri.parse("smsto:"));
        shareIntent.putExtra("sms_body", getLinkToApp());
        getActivity().startActivity(Intent.createChooser(shareIntent, getString(R.string.share)));
    }

    private void copyToClipboard() {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText(getString(R.string.share), getLinkToApp()));

        Toast.makeText(getActivity(), getString(R.string.share_copied_to_clipboard), Toast.LENGTH_SHORT).show();
    }

    private String getLinkToApp() {
        return "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName();
    }
}
