package ru.baykalapps.base.fragments.auth;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.octo.android.robospice.persistence.DurationInMillis;

import ru.baykalapps.base.R;
import ru.baykalapps.base.activities.AuthActivity;
import ru.baykalapps.base.api.requests.InstallationsCountRequest;
import ru.baykalapps.base.api.requests.LoginRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.fragments.base.BaseSpiceFragment;
import ru.baykalapps.base.model.LoginResponse;
import ru.baykalapps.base.prefs.AuthPref;

public class LoginFragment extends BaseSpiceFragment {

    private String mEmail;
    private String mPassword;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        view.findViewById(R.id.loginButton).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        login();
                    }
                });

        view.findViewById(R.id.registrationButton).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((AuthActivity) getActivity()).navigateToRegistration();
                    }
                });

        return view;
    }

    private boolean isValidForm(View v) {
        boolean valid = true;

        EditText emailEditText = (EditText) v.findViewById(R.id.email);

        String email = emailEditText.getText().toString();
        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.required));
            valid = false;
        } else if (!isValidEmail(email)) {
            emailEditText.setError(getString(R.string.email_invalid_format));
            valid = false;
        } else {
            emailEditText.setError(null);
        }

        EditText passwordEditText = (EditText) v.findViewById(R.id.password);
        String password = passwordEditText.getText().toString();
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError("Обязательное поле");
            valid = false;
        } else {
            passwordEditText.setError(null);
        }

        return valid;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void login() {
        View v = getView();
        if (v == null)
            return;
        if (isValidForm(v)) {
            mEmail = ((EditText) v.findViewById(R.id.email)).getText().toString();
            mPassword = ((EditText) v.findViewById(R.id.password)).getText().toString();

            getSpiceManager().execute(
                    new LoginRequest(mEmail, mPassword),
                    new LoginRequestListener()
            );
        }
    }

    public class LoginRequestListener extends SimplerRequestListener<LoginResponse> {

        @Override
        public void onRequestSuccess(LoginResponse response) {
            if (response.getToken() != null) {
                AuthPref.setAuthToken(response.getAuthToken());
                AuthPref.setUserName(response.getUsername());
                AuthPref.setPassword(mPassword);
                AuthPref.setEmail(mEmail);
                AuthPref.setAvatar(response.getAvatar());

                getSpiceManager().execute(new InstallationsCountRequest(), "Installations", DurationInMillis.ONE_MINUTE, null);

                getActivity().finish();
            } else {
                int error = getResources().getIdentifier(response.getStatus(), "string", getActivity().getPackageName());
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        }
    }


}
