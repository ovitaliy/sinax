package ru.baykalapps.base.fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.events.RevisionCheckEvent;
import ru.baykalapps.base.events.ThemeChangeEvent;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.prefs.AppPrefs;
import ru.baykalapps.base.utils.UiUtil;
import ru.baykalapps.base.widget.MainButton;
import ru.baykalapps.base.widget.PhotoCardLayout;

public class StartFragment extends BaseMenuFragment {

    private OnStartPageVisibilityChangListener mOnStartPageVisibilityChangedListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        TextView mCardView = ((TextView) view.findViewById(R.id.mcard));
        mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtil.openLink(getActivity(), Constant.CONTACTS.SITE);
            }
        });
        mCardView.setTextColor(
                SinaxApplication.THEME == -1
                        ? getResources().getColor(R.color.main_button_pressed)
                        : getResources().getColor(R.color.mcard_light));

        view.findViewById(R.id.logo).setVisibility(View.GONE);

        return view;
    }

    @SuppressWarnings("unused")
    public void onEvent(RevisionCheckEvent event) {
        if (getView() != null)
            ((PhotoCardLayout) getView().findViewById(R.id.photocard)).reload();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mOnStartPageVisibilityChangedListener != null) {
            mOnStartPageVisibilityChangedListener.onStartPageVisibilityChanged(true);
        }

        EventBus.getDefault().register(this);
    }

    @SuppressWarnings("unused")
    public void onEvent(ThemeChangeEvent event) {
        if (getView() == null)
            return;

        int color = SinaxApplication.THEME == -1 ? Color.WHITE : getResources().getColor(R.color.category_text_color);

        ((MainButton) getView().findViewById(R.id.contacts)).changeColor(color);
        ((MainButton) getView().findViewById(R.id.share)).changeColor(color);
        ((MainButton) getView().findViewById(R.id.openMenu)).changeColor(color);
        ((MainButton) getView().findViewById(R.id.openNotes)).changeColor(color);
        ((TextView) getView().findViewById(R.id.mcard)).setTextColor(
                SinaxApplication.THEME == -1
                        ? getResources().getColor(R.color.main_button_pressed)
                        : getResources().getColor(R.color.mcard_light));

        getActivity().supportInvalidateOptionsMenu();

        ((PhotoCardLayout) getView().findViewById(R.id.photocard)).invalidateForTheme();

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (mOnStartPageVisibilityChangedListener != null) {
            mOnStartPageVisibilityChangedListener.onStartPageVisibilityChanged(false);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnStartPageVisibilityChangedListener = (OnStartPageVisibilityChangListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnStartPageVisibilityChangedListener = null;
    }


    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main_menu, menu);

        menu.findItem(R.id.toolbar_switch_theme).setVisible(SinaxApplication.THEME == -1);
        menu.findItem(R.id.toolbar_switch_theme_light).setVisible(SinaxApplication.THEME == 1);

        int langIcon = getResources().getIdentifier("ic_toolbar_switch_lang_" + AppPrefs.getLanguage(), "drawable", getActivity().getPackageName());
        menu.findItem(R.id.toolbar_switch_lang).setIcon(langIcon);

        getActivity().setTitle(R.string.app_name_full);
        return true;
    }

    public interface OnStartPageVisibilityChangListener {
        void onStartPageVisibilityChanged(boolean visible);
    }
}
