package ru.baykalapps.base.fragments.base;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;

/**
 * Created by ovitali on 08.07.2015.
 * Project is Sinax
 */
public abstract class BaseMenuFragment extends Fragment {

    public abstract boolean createMenu(Menu menu, MenuInflater inflater);


    @Override
    public void onStart() {
        super.onStart();
        Toolbar toolbar = ((Toolbar) (getActivity()).findViewById(R.id.toolbar));
        Drawable drawable = toolbar.getNavigationIcon();
        if (drawable != null) {
            if (SinaxApplication.THEME == 1)
                drawable.setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
            else
                drawable.clearColorFilter();
        }

        getActivity().supportInvalidateOptionsMenu();
    }


}
