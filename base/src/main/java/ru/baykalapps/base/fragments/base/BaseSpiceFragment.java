package ru.baykalapps.base.fragments.base;

import android.support.v4.app.Fragment;

import com.octo.android.robospice.SpiceManager;

import ru.baykalapps.base.api.ApiSpiceService;

/**
 * Created by ovitali on 08.07.2015.
 * Project is Sinax
 */
public abstract class BaseSpiceFragment extends Fragment {

    private SpiceManager spiceManager = null;

    @Override
    public void onStop() {
        try {
            if (spiceManager != null)
                spiceManager.shouldStop();
        }catch (Exception ignore){}
        super.onStop();
    }


    protected final SpiceManager getSpiceManager() {
        if (spiceManager == null) {
            spiceManager = new SpiceManager(ApiSpiceService.class);
            spiceManager.start(getActivity());
        }

        return spiceManager;
    }

}
