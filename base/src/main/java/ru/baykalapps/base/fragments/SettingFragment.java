package ru.baykalapps.base.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.dialogs.ConfirmCacheDialog;
import ru.baykalapps.base.dialogs.SelectFontSizeDialog;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.utils.UiUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseMenuFragment implements View.OnClickListener {


    public SettingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_setting, container, false);

        for (int i = 0; i < view.getChildCount(); i++) {
            View v = view.getChildAt(i);
            if (v instanceof Button) {
                Button button = (Button) v;
                button.setOnClickListener(this);

                if (SinaxApplication.THEME == 1) {
                    button.setTextColor(SinaxApplication.GREY_COLOR);
                }

            }
        }
        view.findViewById(R.id.cache).setOnClickListener(this);
        view.findViewById(R.id.clear_cache).setOnClickListener(this);
        return view;
    }

    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().setTitle(R.string.action_settings);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        new GetCacheSizes().execute();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.cache) {
            ConfirmCacheDialog.show(getActivity());

        } else if (i == R.id.clear_cache) {
            new ClearCache().execute();

        } else if (i == R.id.fontSize) {
            SelectFontSizeDialog.show(getActivity());

        } else if (i == R.id.about) {
            UiUtil.openLink(getActivity(), Constant.CONTACTS.ABOUT_APP);

        } else if (i == R.id.instruction) {
            UiUtil.openLink(getActivity(), Constant.CONTACTS.INSTRUCTION);

        }
    }

    private class ClearCache extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            assert getActivity() != null;
            assert getActivity().getExternalCacheDir() != null;

            File[] list = getActivity().getExternalCacheDir().listFiles();
            if (list != null) {
                for (File f : list) {
                    if (f.isDirectory()) {
                        recursiveDelete(f);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            new GetCacheSizes().execute();
        }

        void recursiveDelete(File file) {
            if (!file.exists())
                return;

            if (file.isDirectory()) {
                for (File f : file.listFiles()) {
                    recursiveDelete(f);
                }
            }

            file.delete();
        }
    }

    private class GetCacheSizes extends AsyncTask<Void, Void, Float> {
        @Override
        protected Float doInBackground(Void... params) {
            File files = getActivity().getExternalCacheDir();

            long size = 0;
            if (files != null) {
                for (File f : files.listFiles()) {
                    if (f.isDirectory())
                        size += getSizes(f);
                }
            }

            return size / 1024f / 1024f;
        }

        @Override
        protected void onPostExecute(Float size) {
            super.onPostExecute(size);

            if (getView() == null) return;

            String text = getString(R.string.clear_cache, size);
            ((Button) getView().findViewById(R.id.clear_cache)).setText(text);
        }

        private long getSizes(File f) {
            if (f.isFile()) {
                return f.length();
            }

            long size = 0;
            for (File file : f.listFiles()) {
                if (file.isFile()) {
                    size += file.length();
                } else {
                    size += getSizes(f);
                }
            }
            return size;
        }
    }
}
