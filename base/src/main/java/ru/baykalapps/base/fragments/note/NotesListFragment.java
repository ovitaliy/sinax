package ru.baykalapps.base.fragments.note;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.adapter.NotesAdapter;
import ru.baykalapps.base.db.callbacks.SimpleLoaderCallbacks;
import ru.baykalapps.base.db.loaders.NotesLoader;
import ru.baykalapps.base.events.NoteEvent;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.model.Note;
import ru.baykalapps.base.utils.UiUtil;

public class NotesListFragment extends BaseMenuFragment {

    public static NotesListFragment newInstance() {
        NotesListFragment fragment = new NotesListFragment();
        return fragment;
    }

    private List<Note> mNoteList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getLoaderManager().initLoader(Constant.LOADER.NOTES, Bundle.EMPTY, new NoteLoaderCallback());
        return inflater.inflate(R.layout.fragment_notes_list, container, false);
    }


    public void onEvent(NoteEvent event) {
        switch (event.getEventType()) {
            case RELOAD:
                getLoaderManager().restartLoader(Constant.LOADER.NOTES, Bundle.EMPTY, new NoteLoaderCallback());
                break;
        }
    }


    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().setTitle(R.string.notes);
        inflater.inflate(R.menu.notes_list, menu);

        if (SinaxApplication.THEME == 1) {
            menu.findItem(R.id.toolbarAddNote).getIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
            menu.findItem(R.id.toolbar_share).getIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
        } else {
            menu.findItem(R.id.toolbarAddNote).getIcon().setColorFilter(null);
            menu.findItem(R.id.toolbar_share).getIcon().setColorFilter(null);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.toolbar_share) {
            share();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void share() {
        Context context = getActivity();
        String title = context.getString(R.string.share_notes);

        String text = getString(R.string.notes) + "\n";

        for (Note note : mNoteList) {
            text += Constant.DATE_FORMAT.NOTE_SHARE.format(new Date(note.getCreated())) + ": " + note.getNote() + "\n";
        }
        UiUtil.share(context, title, text);
    }

    private class NoteLoaderCallback extends SimpleLoaderCallbacks<List<Note>> {
        @Override
        public Loader<List<Note>> onCreateLoader(int id, Bundle args) {
            return new NotesLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<List<Note>> loader, List<Note> data) {
            View view = getView();
            if (view == null)
                return;

            mNoteList = data;

            SuperRecyclerView recyclerView = (SuperRecyclerView) view.findViewById(R.id.list);

            SparseItemRemoveAnimator mSparseAnimator = new SparseItemRemoveAnimator();
            recyclerView.getRecyclerView().setItemAnimator(mSparseAnimator);

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            recyclerView.setAdapter(new NotesAdapter(data));
        }
    }

}
