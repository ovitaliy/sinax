package ru.baykalapps.base.fragments.note;


import android.app.Fragment;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.events.NoteEvent;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.model.Note;
import ru.baykalapps.base.utils.UiUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteFragment extends BaseMenuFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_note, container, false);
    }

    private void save() {
        View v = getView();
        if (v == null)
            return;

        EditText editText = (EditText) v.findViewById(R.id.note);

        String noteText = editText.getText().toString();

        if (TextUtils.isEmpty(noteText)) {
            editText.setError(getString(R.string.error_empty_note));
        }
        Note note = new Note();
        note.setCreated(System.currentTimeMillis());
        note.setNote(noteText);

        UiUtil.hideKeyboard(editText);
        getActivity().onBackPressed();

        new SaveNoteTask().execute(note);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.toolbar_save_note) {
            save();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().setTitle(R.string.toolbar_add_note);
        inflater.inflate(R.menu.notes_add, menu);

        if (SinaxApplication.THEME == 1)
            menu.findItem(R.id.toolbar_save_note).getIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
        else
            menu.findItem(R.id.toolbar_save_note).getIcon().setColorFilter(null);


        return true;
    }

    private static class SaveNoteTask extends AsyncTask<Note, Void, Void> {
        @Override
        protected Void doInBackground(Note... params) {
            SinaxApplication.getDatabaseDao().getNoteDao().insert(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            EventBus.getDefault().post(new NoteEvent());
        }
    }
}
