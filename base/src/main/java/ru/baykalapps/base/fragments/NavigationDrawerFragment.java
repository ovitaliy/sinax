package ru.baykalapps.base.fragments;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.List;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.activities.MainActivity;
import ru.baykalapps.base.db.callbacks.SimpleLoaderCallbacks;
import ru.baykalapps.base.db.loaders.CategoryLoader;
import ru.baykalapps.base.events.RevisionCheckEvent;
import ru.baykalapps.base.fragments.base.BaseSpiceFragment;
import ru.baykalapps.base.model.Category;
import ru.baykalapps.base.utils.ConverterUtil;
import ru.baykalapps.base.utils.UiUtil;

public class NavigationDrawerFragment extends BaseSpiceFragment implements View.OnClickListener {

    private NavigationDrawerCallbacks mCallbacks;

    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;

    private RelativeLayout mContentFrame;
    private float lastTranslate = 0.0f;

    private boolean mFirstLaunch = true;

    public NavigationDrawerFragment() {
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        ((TextView) view.findViewById(R.id.hotline)).setTextColor(
                SinaxApplication.THEME == -1 ? Color.WHITE : getResources().getColor(R.color.mcard_light)
        );

        return view;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void close() {
        if (isDrawerOpen()) {
            mDrawerLayout.closeDrawers();
        }
    }

    public void showDrawerToggle(boolean showDrawerIndicator) {
        mDrawerToggle.setDrawerIndicatorEnabled(showDrawerIndicator);
    }

    public void toggle() {
        if (isDrawerOpen()) {
            mDrawerLayout.closeDrawers();
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void fillCategoryList(List<Category> categoryList) {
        View v = getView();
        if (v == null) {
            return;
        }
        final LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.categoriesHolder);
        while (linearLayout.getChildCount() > 1)
            linearLayout.removeViewAt(0);

        final View developerView = v.findViewById(R.id.developer);
        UiUtil.removeMeFromParent(developerView);

        final LayoutInflater inflater = LayoutInflater.from(getActivity());

        final int color = SinaxApplication.THEME == -1 ? getResources().getColor(R.color.main_button_pressed) : SinaxApplication.GREY_COLOR;

        final Drawable settingDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.login_btn);
        settingDrawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);

        TextView textView = (TextView) inflater.inflate(R.layout.item_category, linearLayout, false);
        textView.setText(R.string.action_settings);
        textView.setCompoundDrawablesWithIntrinsicBounds(settingDrawable, null, null, null);
        textView.setCompoundDrawablePadding((int) ConverterUtil.dpToPix(getActivity(), 5));
        textView.setOnClickListener(this);
        textView.setTextColor(color);
        textView.setAllCaps(true);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        linearLayout.addView(textView);
        linearLayout.addView(createLineView());

        for (int i = 0; i < categoryList.size(); i++) {
            textView = (TextView) inflater.inflate(R.layout.item_category, linearLayout, false);
            Category category = categoryList.get(i);
            textView.setText(category.getTitle());
            textView.setTag(category);
            textView.setTextColor(color);
            textView.setOnClickListener(this);

            linearLayout.addView(textView);

            if (i == 0 || i == categoryList.size() - 4) {
                linearLayout.addView(createLineView());
            }

        }
        linearLayout.addView(developerView);

        linearLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                ScrollView scrollView = (ScrollView) linearLayout.getParent();
                scrollView.scrollTo(0, (int) ConverterUtil.dpToPix(getActivity(), 38));
            }
        }, 100);

    }

    private View createLineView() {
        View line = new View(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, (int) ConverterUtil.dpToPix(getActivity(), 1));
        line.setLayoutParams(layoutParams);

        int padding = (int) ConverterUtil.dpToPix(getActivity(), 15);
        layoutParams.leftMargin = padding;
        layoutParams.rightMargin = padding;

        line.setBackgroundColor(getResources().getColor(R.color.gray_dark));

        return line;

    }

    public void changeTheme() {

        View v = getView();
        if (v == null) {
            return;
        }
        getLoaderManager().restartLoader(Constant.LOADER.CATEGORY, Bundle.EMPTY, new CategoryLoaderCallback());

        Drawable hotlineBackgroundDrawable = ContextCompat.getDrawable(getActivity(),
                SinaxApplication.THEME == 1 ? R.drawable.toolbar_light : R.drawable.toolbar_dark
        );

      /*  Drawable background = SinaxApplication.THEME == 1
                ? ContextCompat.getDrawable(getActivity(), R.drawable.background_light)
                : null;*/

        ((TextView) v.findViewById(R.id.hotline)).setTextColor(
                SinaxApplication.THEME == -1 ? Color.WHITE : getResources().getColor(R.color.main_button_pressed)
        );

        //UiUtil.setBackgroundDrawable(v, background);
        UiUtil.setBackgroundDrawable(v.findViewById(R.id.hotline), hotlineBackgroundDrawable);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, int contentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mContentFrame = (RelativeLayout) getActivity().findViewById(contentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        // set up the drawer's list view with items and click listener
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);
        mDrawerLayout.setBackgroundColor(Color.TRANSPARENT);

        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                (Toolbar) getActivity().findViewById(R.id.toolbar),
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (drawerView.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mContentFrame.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    mContentFrame.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() instanceof Category) {
            Category category = (Category) v.getTag();
            if (mCallbacks != null) {
                mCallbacks.onNavigationDrawerItemSelected(category.getId(), category.getTitle());
            }
        } else {
            ((MainActivity) getActivity()).startFragment(new SettingFragment(), true);
            close();
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(RevisionCheckEvent event) {
        getLoaderManager().restartLoader(Constant.LOADER.CATEGORY, Bundle.EMPTY, new CategoryLoaderCallback());
    }

    public class CategoryLoaderCallback extends SimpleLoaderCallbacks<List<Category>> {

        @Override
        public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
            return new CategoryLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {

            if (data != null && data.size() != 0) {
                fillCategoryList(data);
            }

        }
    }

    public interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(long categoryId, String categoryName);
    }
}
