package ru.baykalapps.base.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import ru.baykalapps.base.R;
import ru.baykalapps.base.activities.AuthActivity;
import ru.baykalapps.base.api.requests.PostAvatarRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.dialogs.InAppAvatarSourceDialog;
import ru.baykalapps.base.dialogs.SelectImageSourceDialog;
import ru.baykalapps.base.fragments.base.BaseSpiceFragment;
import ru.baykalapps.base.model.RegistrationResponse;
import ru.baykalapps.base.prefs.AuthPref;

public class LogoutFragment extends BaseSpiceFragment implements View.OnClickListener {

    private ImageView mAvatarView;

    private String mAvatar;

    private File mCaptureImageFileUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logout, container, false);

        TextView textView = (TextView) view.findViewById(R.id.username);
        textView.setText(getString(R.string.logged_in_info, AuthPref.getEmail()));

        mAvatarView = (ImageView) view.findViewById(R.id.avatar);
        mAvatarView.setOnClickListener(this);

        ImageLoader.getInstance().displayImage(AuthPref.getAvatar(), (ImageView) view.findViewById(R.id.avatar));

        view.findViewById(R.id.logoutButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthPref.setAuthToken(null);
                AuthPref.setAvatar(null);
                AuthPref.setEmail(null);
                AuthPref.setPassword(null);

                ((AuthActivity) getActivity()).navigateToLogin();
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.avatar) {
            SelectImageSourceDialog.show(getActivity(), this);

        } else if (i == R.id.source_gallery) {
            pickImage();

        } else if (i == R.id.source_camera) {
            captureImage();

        } else if (i == R.id.source_app) {
            final InAppAvatarSourceDialog inAppAvatarSourceDialog = new InAppAvatarSourceDialog();
            inAppAvatarSourceDialog.setOnImageSelectListener(new InAppAvatarSourceDialog.OnImageSelectListener() {
                @Override
                public void onImageSelected(String selectedUrl) {
                    inAppAvatarSourceDialog.dismiss();
                    mAvatar = selectedUrl;
                    loadAvatar();
                }
            });
            getFragmentManager().beginTransaction().add(inAppAvatarSourceDialog, "inAppAvatarSourceDialog").commit();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RegistrationFragment.PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }

            mAvatar = data.getData().toString();
            loadAvatar();
        } else if (requestCode == RegistrationFragment.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mAvatar = "file:" + mCaptureImageFileUri.getAbsolutePath();
            loadAvatar();
        }
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, RegistrationFragment.PICK_PHOTO_FOR_AVATAR);
    }

    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            mCaptureImageFileUri = File.createTempFile("image", ".jpg", getActivity().getExternalCacheDir());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCaptureImageFileUri)); // set the image file name
        } catch (Exception ignore) {
        }
        // start the image capture Intent
        startActivityForResult(intent, RegistrationFragment.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    public void loadAvatar() {
        ImageLoader.getInstance().displayImage(mAvatar, mAvatarView);
        mAvatarView.setOnClickListener(this);
        uploadAvatar();
    }

    private void uploadAvatar() {
        try {
            File sendedFile = new File(getActivity().getExternalCacheDir(), "tmp.png");
            sendedFile.deleteOnExit();
            FileOutputStream fileOutputStream = new FileOutputStream(sendedFile);
            if (mAvatar.contains("assets://"))
                IOUtils.copy(getActivity().getAssets().open(mAvatar.replace("assets://", "")), fileOutputStream);
            else if (mAvatar.contains("content://")) {
                IOUtils.copy(getActivity().getContentResolver().openInputStream(Uri.parse(mAvatar)), fileOutputStream);
            } else {
                File sourceFile = new File(mAvatar.replace("file:", ""));
                IOUtils.copy(new FileInputStream(sourceFile), fileOutputStream);
            }

            getSpiceManager().execute(new PostAvatarRequest(sendedFile, AuthPref.getEmail()), new PostAvatarListener());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private final class PostAvatarListener extends SimplerRequestListener<RegistrationResponse> {
        @Override
        public void onRequestSuccess(RegistrationResponse baseResponse) {
            System.out.print(baseResponse);
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            super.onRequestFailure(spiceException);
        }
    }


}
