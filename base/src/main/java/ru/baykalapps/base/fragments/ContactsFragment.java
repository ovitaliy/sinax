package ru.baykalapps.base.fragments;


import android.app.Fragment;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.List;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.adapter.ContactsAdapter;
import ru.baykalapps.base.adapter.ContactsLightAdapter;
import ru.baykalapps.base.adapter.decoration.ItemPaddingDecoration;
import ru.baykalapps.base.api.requests.ContactRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.db.callbacks.SimpleLoaderCallbacks;
import ru.baykalapps.base.db.loaders.ContactsLoader;
import ru.baykalapps.base.fragments.base.BaseMenuSpiceFragment;
import ru.baykalapps.base.model.Contact;
import ru.baykalapps.base.model.ContactRow;
import ru.baykalapps.base.model.ContactsList;
import ru.baykalapps.base.utils.ConverterUtil;
import ru.baykalapps.base.utils.UiUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends BaseMenuSpiceFragment implements View.OnClickListener {


    private List<Contact> mContactList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getLoaderManager().initLoader(Constant.LOADER.CONTACTS, Bundle.EMPTY, new ContactsLoaderCallback());

        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().setTitle(R.string.contacts);

        inflater.inflate(R.menu.contacts, menu);

        if (SinaxApplication.THEME == 1) {
            menu.findItem(R.id.toolbar_share).getIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
        } else {
            menu.findItem(R.id.toolbar_share).getIcon().setColorFilter(null);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.toolbar_share) {
            share();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void share() {
        String share = "";
        Context context = getActivity();
        for (Contact contact : mContactList) {

            int contactTypeId = context.getResources().getIdentifier("contacts_" + contact.getType(), "string", context.getPackageName());
            share += getString(contactTypeId) + ":\n";

            for (ContactRow contactRow : contact.getRows()) {
                if (contactRow.getTitle() != null)
                    share += contactRow.getTitle() + ":" + contactRow.getValue();
                else
                    share += contactRow.getValue();

                share += "\n\n";
            }
        }

        UiUtil.share(context, getString(R.string.share), share);
    }

    private void showContacts(List<Contact> list) {
        View v = getView();
        if (v == null) {
            return;
        }

        mContactList = list;

        SuperRecyclerView recyclerView = (SuperRecyclerView) v.findViewById(R.id.contactsList);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (SinaxApplication.THEME == -1) {
            recyclerView.addItemDecoration(new ItemPaddingDecoration(ConverterUtil.dpToPix(getActivity(), 15)));
            recyclerView.setAdapter(new ContactsAdapter(list, this));
        } else {
            recyclerView.setAdapter(new ContactsLightAdapter(list, this));
        }
    }

    @Override
    public void onClick(View v) {

        String value = v.getTag(R.string.contact_value).toString();
        String type = v.getTag(R.string.contact_type).toString();

        String url = value;

        switch (type) {
            case "email":
                url = "mailto:" + value;
                break;
            case "phone":
                url = "tel:" + value;
                break;
            case "skype":
                url = "skype:" + value;
                break;
            case "fb":
                url = value.replace("fb://profile/", "http://facebook.com/");
                break;
        }

        UiUtil.openLink(getActivity(), url);
    }


    private class ContactsLoaderCallback extends SimpleLoaderCallbacks<List<Contact>> {
        @Override
        public Loader<List<Contact>> onCreateLoader(int id, Bundle args) {
            return new ContactsLoader(getActivity());
        }

        @Override
        public void onLoadFinished(Loader<List<Contact>> loader, List<Contact> data) {
            if (data == null || data.size() == 0) {
                getSpiceManager().execute(
                        new ContactRequest(),
                        "ContactRequest",
                        DurationInMillis.ONE_MINUTE,
                        new ContactRequestListener()
                );
            } else {
                showContacts(data);
            }
        }
    }

    //--------------
    private class ContactRequestListener extends SimplerRequestListener<ContactsList> {
        @Override
        public void onRequestSuccess(ContactsList contacts) {
            showContacts(contacts);
        }
    }


}
