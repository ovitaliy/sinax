package ru.baykalapps.base.fragments.base;

import com.octo.android.robospice.SpiceManager;

import ru.baykalapps.base.api.ApiSpiceService;

/**
 * Created by ovitali on 08.07.2015.
 * Project is Sinax
 */
public abstract class BaseMenuSpiceFragment extends BaseMenuFragment {

    private SpiceManager spiceManager = null;

    @Override
    public void onStop() {
        if (spiceManager != null && spiceManager.isStarted()) {
            spiceManager.shouldStop();
            spiceManager = null;
        }
        super.onStop();
    }


    protected final SpiceManager getSpiceManager() {
        if (spiceManager == null) {
            spiceManager = new SpiceManager(ApiSpiceService.class);
            spiceManager.start(getActivity());
        }

        return spiceManager;
    }

}
