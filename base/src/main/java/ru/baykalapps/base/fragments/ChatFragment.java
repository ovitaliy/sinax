package ru.baykalapps.base.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.Date;
import java.util.List;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.adapter.ChatMessageAdapter;
import ru.baykalapps.base.api.requests.GetChatMessagesListRequest;
import ru.baykalapps.base.api.requests.PostMessageRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.db.callbacks.SimpleLoaderCallbacks;
import ru.baykalapps.base.db.loaders.ChatMessageLoader;
import ru.baykalapps.base.dialogs.ErrorDialog;
import ru.baykalapps.base.fragments.base.BaseMenuSpiceFragment;
import ru.baykalapps.base.model.BaseResponse;
import ru.baykalapps.base.model.ChatMessage;
import ru.baykalapps.base.model.ChatMessageList;
import ru.baykalapps.base.prefs.AuthPref;
import ru.baykalapps.base.utils.UiUtil;

/**
 * Created by ovitali on 15.07.2015.
 * Project is Sinax
 */
public class ChatFragment extends BaseMenuSpiceFragment implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private ChatMessageAdapter mChatMessageAdapter;

    private String mSelectedUserName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        view.findViewById(R.id.send).setOnClickListener(this);

        getSpiceManager().execute(new GetChatMessagesListRequest(), new ChatMessageResponseListener());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = new Bundle(1);
        if (mSelectedUserName != null)
            bundle.putString("name", mSelectedUserName);

        getLoaderManager().initLoader(Constant.LOADER.CHAT, bundle, new ChatMessageLoaderCallback());
    }

    @Override
    public boolean createMenu(Menu menu, MenuInflater inflater) {
        getActivity().setTitle(R.string.hotline);

        menu.clear();
        inflater.inflate(R.menu.chat, menu);

        boolean loggedIn = AuthPref.getAuthToken() != null;

        menu.findItem(R.id.auth).setVisible(!loggedIn);
        menu.findItem(R.id.logout).setVisible(loggedIn);

        return true;
    }

    @Override
    public void onClick(View v) {
        v = getView();
        if (v == null) {
            return;
        }

        if (AuthPref.getAuthToken() == null) {
            ErrorDialog.showUnauthorizedError(getActivity());
            return;
        }

        EditText messageView = ((EditText) v.findViewById(R.id.newMessage));
        String message = messageView.getText().toString();

        if (TextUtils.isEmpty(message)) {
            Toast.makeText(getActivity(), R.string.error_blank_message, Toast.LENGTH_SHORT).show();
            return;
        }

        messageView.setText("");

        try {

            UiUtil.hideKeyboard(messageView);

            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setEmail(AuthPref.getEmail());
            chatMessage.setAvatar(AuthPref.getAvatar());
            chatMessage.setUsername(AuthPref.getUserName());
            chatMessage.setText(message);
            chatMessage.setAdmin(false);
            chatMessage.setJustDate(false);
            chatMessage.setDate(new Date());

            SinaxApplication.getDatabaseDao().getChatMessageDao().insert(chatMessage);

            message = unicodeString(message);

            getLoaderManager().restartLoader(Constant.LOADER.CHAT, Bundle.EMPTY, new ChatMessageLoaderCallback());

            getSpiceManager().execute(new PostMessageRequest(message), new PostChatMessageResponseListener());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private class ChatMessageLoaderCallback extends SimpleLoaderCallbacks<List<ChatMessage>> {
        @Override
        public Loader<List<ChatMessage>> onCreateLoader(int id, Bundle args) {
            String userName = null;
            if (args.containsKey("name"))
                userName = args.getString("name");
            return new ChatMessageLoader(getActivity(), userName);
        }

        @Override
        public void onLoadFinished(Loader<List<ChatMessage>> loader, List<ChatMessage> data) {
            if (getView() == null) {
                return;
            }

            if (mChatMessageAdapter == null) {
                mChatMessageAdapter = new ChatMessageAdapter(getActivity(), data, mOnAvatarClick);
                SuperRecyclerView recyclerView = (SuperRecyclerView) getView().findViewById(R.id.message_list);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(mChatMessageAdapter);
            } else {
                mChatMessageAdapter.setData(data);
            }
        }
    }

    private View.OnClickListener mOnAvatarClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (mSelectedUserName == null) {
                ChatMessage chatMessage = (ChatMessage) v.getTag();
                if (chatMessage.getAdmin()) {
                    mSelectedUserName = "admin";
                } else {
                    mSelectedUserName = chatMessage.getUsername();
                }
            } else {
                mSelectedUserName = null;
            }
            Bundle bundle = new Bundle(1);
            if (mSelectedUserName != null)
                bundle.putString("name", mSelectedUserName);

            getLoaderManager().restartLoader(Constant.LOADER.CHAT, bundle, new ChatMessageLoaderCallback());


        }
    };


    private class ChatMessageResponseListener extends SimplerRequestListener<ChatMessageList> {

        @Override
        public void onRequestSuccess(ChatMessageList o) {
            getLoaderManager().restartLoader(Constant.LOADER.CHAT, Bundle.EMPTY, new ChatMessageLoaderCallback());
        }
    }

    private class PostChatMessageResponseListener extends SimplerRequestListener<BaseResponse> {
        @Override
        public void onRequestSuccess(BaseResponse baseResponse) {
            System.out.print(baseResponse);

            //reload
            getSpiceManager().execute(new GetChatMessagesListRequest(), new ChatMessageResponseListener());

        }
    }

    public static String unicodeEscaped(char ch) {
        if (ch < 0x10) {
            return "\\u000" + Integer.toHexString(ch);
        } else if (ch < 0x100) {
            return "\\u00" + Integer.toHexString(ch);
        } else if (ch < 0x1000) {
            return "\\u0" + Integer.toHexString(ch);
        }
        return "\\u" + Integer.toHexString(ch);
    }

    public static String unicodeString(String message) {
        String result = "";
        for (int i = 0; i < message.length(); i++) {
            result += unicodeEscaped(message.charAt(i));
        }
        return result;
    }


}
