package ru.baykalapps.base.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.activities.AuthActivity;
import ru.baykalapps.base.api.requests.PostAvatarRequest;
import ru.baykalapps.base.api.requests.RegistrationRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.dialogs.InAppAvatarSourceDialog;
import ru.baykalapps.base.dialogs.SelectImageSourceDialog;
import ru.baykalapps.base.fragments.base.BaseSpiceFragment;
import ru.baykalapps.base.model.RegistrationResponse;

public class RegistrationFragment extends BaseSpiceFragment implements View.OnClickListener {

    public static final int PICK_PHOTO_FOR_AVATAR = 100;
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 101;


    private String mUserName;
    private String mEmail;
    private String mPassword;
    private String mAvatar;

    private ImageView mAvatarView;

    private File mCaptureImageFileUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);


        view.findViewById(R.id.cancel).setOnClickListener(this);

        view.findViewById(R.id.loginButton).setOnClickListener(this);


        int index = (int) (Math.random() * Constant.IN_APP_AVATARS_COUNT);

        mAvatarView = (ImageView) view.findViewById(R.id.avatar);

        mAvatar = Constant.getLocalAvatarUrl(index);
        loadAvatar();

        return view;
    }

    private boolean isValidForm(View v) {
        boolean valid = true;

        EditText userNameEditText = (EditText) v.findViewById(R.id.userName);

        String userName = userNameEditText.getText().toString();

        if (TextUtils.isEmpty(userName)) {
            userNameEditText.setError(getString(R.string.required));
            valid = false;
        } else {
            userNameEditText.setError(null);
        }

        EditText emailEditText = (EditText) v.findViewById(R.id.email);

        String email = emailEditText.getText().toString();

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.required));
            valid = false;
        } else if (!isValidEmail(email)) {
            emailEditText.setError(getString(R.string.email_invalid_format));
            valid = false;
        } else {
            emailEditText.setError(null);
        }

        EditText passwordEditText = (EditText) v.findViewById(R.id.password);
        String password = passwordEditText.getText().toString();
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.required));
            valid = false;
        } else {
            passwordEditText.setError(null);
        }
        EditText confirmPasswordEditText = (EditText) v.findViewById(R.id.confirmPassword);
        String confirmPassword = confirmPasswordEditText.getText().toString();
        if (TextUtils.isEmpty(confirmPassword)) {
            confirmPasswordEditText.setError(getString(R.string.required));
            valid = false;
        } else if (!confirmPassword.equals(password)) {
            confirmPasswordEditText.setError(null);
            confirmPasswordEditText.setError(getString(R.string.required_password_mismatch));
            valid = false;
        } else {
            confirmPasswordEditText.setError(null);
        }

        return valid;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void register() {
        View v = getView();
        if (v == null)
            return;
        if (isValidForm(v)) {
            mUserName = ((EditText) getView().findViewById(R.id.userName)).getText().toString();
            mEmail = ((EditText) getView().findViewById(R.id.email)).getText().toString();
            mPassword = ((EditText) getView().findViewById(R.id.password)).getText().toString();

            getSpiceManager().execute(
                    new RegistrationRequest(mEmail, mUserName, mPassword),
                    new RegistrationRequestListener()
            );
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loginButton) {
            register();

        } else if (i == R.id.cancel) {
            ((AuthActivity) getActivity()).navigateToLogin();

        } else if (i == R.id.avatar) {
            SelectImageSourceDialog.show(getActivity(), this);

        } else if (i == R.id.source_gallery) {
            pickImage();

        } else if (i == R.id.source_camera) {
            captureImage();

        } else if (i == R.id.source_app) {
            final InAppAvatarSourceDialog inAppAvatarSourceDialog = new InAppAvatarSourceDialog();
            inAppAvatarSourceDialog.setOnImageSelectListener(new InAppAvatarSourceDialog.OnImageSelectListener() {
                @Override
                public void onImageSelected(String selectedUrl) {
                    inAppAvatarSourceDialog.dismiss();
                    mAvatar = selectedUrl;
                    loadAvatar();
                }
            });
            getFragmentManager().beginTransaction().add(inAppAvatarSourceDialog, "inAppAvatarSourceDialog").commit();

        }
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            mCaptureImageFileUri = File.createTempFile("image", ".jpg", getActivity().getExternalCacheDir());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCaptureImageFileUri)); // set the image file name
        } catch (Exception ignore) {
        }
        // start the image capture Intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }

            mAvatar = data.getData().toString();
            loadAvatar();
        } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mAvatar = "file:" + mCaptureImageFileUri.getAbsolutePath();
            loadAvatar();
        }
    }

    public void loadAvatar() {
        ImageLoader.getInstance().displayImage(mAvatar, mAvatarView);
        mAvatarView.setOnClickListener(this);
    }

    private void uploadAvatar() {
        try {
            File sendedFile = new File(getActivity().getExternalCacheDir(), "tmp.png");
            sendedFile.deleteOnExit();
            FileOutputStream fileOutputStream = new FileOutputStream(sendedFile);
            if (mAvatar.contains("assets://"))
                IOUtils.copy(getActivity().getAssets().open(mAvatar.replace("assets://", "")), fileOutputStream);
            else if (mAvatar.contains("content://")) {
                IOUtils.copy(getActivity().getContentResolver().openInputStream(Uri.parse(mAvatar)), fileOutputStream);
            } else {
                File sourceFile = new File(mAvatar.replace("file:", ""));
                IOUtils.copy(new FileInputStream(sourceFile), fileOutputStream);
            }

            getSpiceManager().execute(new PostAvatarRequest(sendedFile, mEmail), new PostAvatarListener());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public class RegistrationRequestListener extends SimplerRequestListener<RegistrationResponse> {
        @Override
        public void onRequestSuccess(RegistrationResponse response) {

            if (response.getToken() != null) {
                uploadAvatar();

                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.registration))
                        .setMessage(getString(R.string.registration_confirm))
                        .setPositiveButton(getString(android.R.string.ok), null)
                        .show();


                ((AuthActivity) getActivity()).navigateToLogin();

                /*AuthEvent authEvent = new AuthEvent(
                        response.getToken(),
                        mUserName,
                        mPassword,
                        mEmail,
                        mAvatar
                );
                EventBus.getDefault().post(authEvent);*/
            } else {
                int error = getResources().getIdentifier(response.getStatus(), "string", getActivity().getPackageName());
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        }

    }

    private final class PostAvatarListener extends SimplerRequestListener<RegistrationResponse> {
        @Override
        public void onRequestSuccess(RegistrationResponse baseResponse) {
            System.out.print(baseResponse);
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            super.onRequestFailure(spiceException);
        }
    }

}
