package ru.baykalapps.base.prefs;

import ru.baykalapps.base.utils.PrefHelper;

/**
 * Created by ovitali on 13.07.2015.
 * Project is Sinax
 */
public class AuthPref {

    public static String AUTH_TOKEN = "auth_token";
    public static String EMAIL = "email";
    public static String AVATAR = "avatar";
    public static String USER_NAME = "user_name";
    public static String PASSWORD = "password";

    public static void setAuthToken(String token) {
        PrefHelper.setStringPref(AUTH_TOKEN, token);
    }

    public static void setEmail(String email) {
        PrefHelper.setStringPref(EMAIL, email);
    }

    public static void setAvatar(String avatar) {
        PrefHelper.setStringPref(AVATAR, avatar);
    }

    public static void setUserName(String userName) {
        PrefHelper.setStringPref(USER_NAME, userName);
    }

    public static void setPassword(String password) {
        PrefHelper.setStringPref(PASSWORD, password);
    }

    //-------------------------
    public static String getAuthToken() {
        return PrefHelper.getStringPref(AUTH_TOKEN);
    }

    public static String getUserName() {
        return PrefHelper.getStringPref(USER_NAME);
    }

    public static String getPassword() {
        return PrefHelper.getStringPref(PASSWORD);
    }

    public static String getAvatar() {
        return PrefHelper.getStringPref(AVATAR);
    }

    public static String getEmail() {
        return PrefHelper.getStringPref(EMAIL);
    }
}
