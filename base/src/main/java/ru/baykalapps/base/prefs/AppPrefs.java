package ru.baykalapps.base.prefs;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.utils.PrefHelper;

/**
 * Created by ovitali on 11.07.2015.
 * Project is Sinax
 */
public class AppPrefs {

    private static final String LANGUAGE = "language";
    private static final String REVISION = "revision";
    private static final String THEME = "theme";
    private static final String TEXT_SIZE = "text_size";
    private static final String DOWNLOAD_PROPOSITION_WAS_SHOWED = "download_proposition_was_showed";
    private static final String GCM_REGISTERED = "gcm_registered";
    private static final String COUNT_USERS = "count_users";
    private static final String COUNT_INSTALLATIONS = "count_installations";

    public static int getRevision() {
        return PrefHelper.getIntPref(REVISION);
    }

    public static int getTheme() {
        return PrefHelper.getIntPref(THEME);
    }

    public static void setRevision(int revision) {
        PrefHelper.setIntPref(REVISION, revision);
    }

    public static void setTheme(int theme) {
        PrefHelper.setIntPref(THEME, theme);
    }

    public static void setTextSize(int textSize) {
        PrefHelper.setIntPref(TEXT_SIZE, textSize);
    }

    public static int getTextSize() {
        return PrefHelper.getIntPref(TEXT_SIZE, Constant.DEFAULT_TEXT_SIZE);
    }

    public static boolean getGcmRegistered() {
        return PrefHelper.getBooleanPref(GCM_REGISTERED);
    }


    public static int getInstallationsCount() {
        return PrefHelper.getIntPref(COUNT_INSTALLATIONS, 0);
    }

    public static int getUsersCount() {
        return PrefHelper.getIntPref(COUNT_USERS, 0);
    }

    public static void setInstallationsCount(int value) {
        PrefHelper.setIntPref(COUNT_INSTALLATIONS, value);
    }


    public static void setUsersCount(int value) {
        PrefHelper.setIntPref(COUNT_USERS, value);
    }


    public static void setGcmRegistered(boolean value) {
        PrefHelper.setBooleanPref(GCM_REGISTERED, value);
    }

    public static void setDownloadPropositionWasShowed() {
        PrefHelper.setBooleanPref(DOWNLOAD_PROPOSITION_WAS_SHOWED, true);
    }

    public static boolean getDownloadPropositionWasShowed() {
        return PrefHelper.getBooleanPref(DOWNLOAD_PROPOSITION_WAS_SHOWED);
    }

    public static String getLanguage() {
        return PrefHelper.getStringPref(LANGUAGE);
    }

    public static void setLanguage(String value) {
        PrefHelper.setStringPref(LANGUAGE, value);
    }
}
