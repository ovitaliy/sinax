package ru.baykalapps.base.activities.base;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;

import java.util.List;

import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;

public class BaseFragmentActivity extends AppCompatActivity {

    protected Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (SinaxApplication.SCREEN_WIDTH == -1) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            SinaxApplication.SCREEN_WIDTH = size.x;
            SinaxApplication.SCREEN_HEIGHT = size.y;
        }
    }

    public Fragment startFragment(Fragment f, boolean addToBackStack) {
        return startFragment(f, addToBackStack, false);
    }

    public Fragment startFragment(Fragment f, boolean addToBackStack, boolean popBackStack) {
        currentFragment = startFragment(R.id.container, f, addToBackStack, popBackStack);
        return currentFragment;
    }

    protected Fragment startFragment(int viewId, Fragment f, boolean addToBackStack, boolean popBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        String tag = f.getClass().getSimpleName();

        if (popBackStack) {
            if (manager.findFragmentByTag(tag) != null) {
                List<Fragment> fragments = manager.getFragments();
                int index = -1;
                for (int i = 0; i < fragments.size(); i++) {
                    Fragment fr = fragments.get(i);
                    if (fr.getClass().getSimpleName().equals(tag)) {
                        index = i;
                        break;
                    }
                }

                if (index != -1) {
                    int popCount = fragments.size() - index - 1;
                    for (int i = 0; i < popCount; i++) {
                        manager.popBackStackImmediate();
                    }
                    return fragments.get(index);
                }
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(viewId, f, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }

        ft.commit();

        return f;
    }
}
