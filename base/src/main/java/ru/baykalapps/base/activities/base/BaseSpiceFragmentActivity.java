package ru.baykalapps.base.activities.base;

import com.octo.android.robospice.SpiceManager;

import ru.baykalapps.base.api.ApiSpiceService;

/**
 * Created by ovitali on 11.03.2015.
 */
public abstract class BaseSpiceFragmentActivity extends BaseFragmentActivity {

    private SpiceManager spiceManager = new SpiceManager(ApiSpiceService.class);

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }


    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }


}
