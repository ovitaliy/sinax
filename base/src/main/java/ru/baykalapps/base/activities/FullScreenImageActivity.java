package ru.baykalapps.base.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.widget.ScaleImageView;

public class FullScreenImageActivity extends AppCompatActivity {

    public static void startNewInstance(Context context, String position) {
        Intent intent = new Intent(context, FullScreenImageActivity.class);
        intent.putExtra("position", position);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewPager viewPager = new ViewPager(this);

        String position = getIntent().getExtras().getString("position");

        setContentView(viewPager);
        viewPager.setAdapter(new Adapter());

        if (position != null)
            viewPager.setCurrentItem(Integer.parseInt(position));
    }

    private class Adapter extends PagerAdapter {
        @Override
        public int getCount() {
            return SinaxApplication.PHOTO_CARDS.length;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final ScaleImageView imageView = new ScaleImageView(container.getContext());
            imageView.setBackgroundColor(Color.BLACK);
            String url = SinaxApplication.PHOTO_CARDS[position].getPath();

            ImageLoader.getInstance().loadImage(url, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    imageView.setImageBitmap(loadedImage);
                }
            });

            container.addView(imageView, 0);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }

  /*  private class ImageContainer extends FrameLayout implements View.OnTouchListener {

        private float mScale = 1.f;

        private GestureDetector mMoveGestureDetector;
        private ScaleGestureDetector mScaleGestureDetector;
        private Scroller mScroller;

        int loadedCount;

        public ImageContainer(Context context) {
            super(context);

            setOnTouchListener(this);
            DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().cacheOnDisk(true).build();

            int x = 0;

            for (CardImage cardImage : SinaxApplication.PHOTO_CARDS) {
                ImageView imageView = new ImageView(context);
                imageView.setScaleType(ImageView.ScaleType.MATRIX);
                imageView.setX(x);

                ImageLoader.getInstance().displayImage(cardImage.getPath(), imageView, displayImageOptions, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        loadedCount++;
                        if (loadedCount >= SinaxApplication.PHOTO_CARDS.length) {
                            invalidateSizes();
                        }
                    }
                });
                x += 1242;

                addViewInLayout(imageView, -1, new ViewGroup.LayoutParams(1242, 1200));
            }
            mScroller = new Scroller(context, new DecelerateInterpolator());
            mMoveGestureDetector = new GestureDetector(context, moveGestureListener);
            mScaleGestureDetector = new ScaleGestureDetector(context, scaleGestureListener);

        }

        private void invalidateSizes() {
            float x = 0;
            for (int i = 0; i < getChildCount(); i++) {
                ImageView v = (ImageView) getChildAt(i);
                v.setX(x);
                float w = v.getDrawable().getIntrinsicWidth() * mScale;
                v.getLayoutParams().width = (int) w;
                x += w;
            }

            getLayoutParams().width = (int) x;
        }


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mScaleGestureDetector.onTouchEvent(event);
            mMoveGestureDetector.onTouchEvent(event);

            return true;
        }

        // ScaleGestureDetector.SimpleOnScaleGestureListener

        private final GestureDetector.SimpleOnGestureListener moveGestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                mScroller.forceFinished(true);
                mScroller.startScroll(mScroller.getCurrX(), mScroller.getCurrY(), (int) distanceX, (int) distanceY, 0);
                post(new FlyingRunnable());
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                mScroller.forceFinished(true);
                mScroller.fling(mScroller.getCurrX(), mScroller.getCurrY(),
                        -(int) velocityX, (int) velocityY,
                        0, getLayoutParams().width,
                        0, getLayoutParams().height);

                post(new FlyingRunnable());
                return true;
            }
        };


        private final ScaleGestureDetector.SimpleOnScaleGestureListener scaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                float scale = mScale * detector.getScaleFactor();
                scale = Math.min(3f, scale);
                scale = Math.max(1f, scale);
                mScale = scale;

//                setScaleX(mScale);
//                setScaleY(mScale);

                float x = 0;
                for (int i = 0; i < getChildCount(); i++) {
                    ImageView imageView = (ImageView) getChildAt(i);

                    Drawable d = imageView.getDrawable();

                    imageView.setX(x);

                    x += imageView.getLayoutParams().width = (int) (d.getIntrinsicWidth() * scale);
                    imageView.getLayoutParams().height = (int) (d.getIntrinsicHeight() * scale);

                    Matrix matrix = new Matrix();
                    matrix.setScale(scale, scale);
                    imageView.setImageMatrix(matrix);
                }

                return true;
            }
        };

        private class FlyingRunnable implements Runnable {
            @Override
            public void run() {
                if (mScroller.computeScrollOffset()) {
                    setX(-mScroller.getCurrX());
                    setY(-mScroller.getCurrY());
                    postDelayed(this, 30);
                }
            }
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int w = 0;
            int h = MeasureSpec.getSize(heightMeasureSpec);

            int count = getChildCount();
            for (int i = 0; i < count; i++) {
                ImageView imageView = (ImageView) getChildAt(i);
                Drawable d = imageView.getDrawable();

                float cw = 0;
                float ch = 0;
                if (d != null) {
                    cw = d.getIntrinsicWidth() * mScale;
                    ch = d.getIntrinsicHeight() * mScale;

                    w += cw;
                }
                measureChild(imageView,
                        MeasureSpec.makeMeasureSpec((int) cw, MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec((int) ch, MeasureSpec.EXACTLY)
                );
            }

            setMeasuredDimension(
                    MeasureSpec.makeMeasureSpec(w, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY)
            );

            mScroller.setFinalX(w);
            mScroller.setFinalY(h);
        }
    }*/

}
