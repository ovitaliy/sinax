package ru.baykalapps.base.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.List;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.activities.base.BaseSpiceFragmentActivity;
import ru.baykalapps.base.api.requests.InstallationsCountRequest;
import ru.baykalapps.base.api.requests.RegisterDeviceRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.dialogs.ConfirmCacheDialog;
import ru.baykalapps.base.events.ReceiveTokenEvent;
import ru.baykalapps.base.events.RevisionCheckEvent;
import ru.baykalapps.base.events.ThemeChangeEvent;
import ru.baykalapps.base.fragments.ContactsFragment;
import ru.baykalapps.base.fragments.NavigationDrawerFragment;
import ru.baykalapps.base.fragments.SettingFragment;
import ru.baykalapps.base.fragments.ShareFragment;
import ru.baykalapps.base.fragments.StartFragment;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.fragments.note.NoteFragment;
import ru.baykalapps.base.fragments.note.NotesListFragment;
import ru.baykalapps.base.gcm.RegistrationIntentService;
import ru.baykalapps.base.model.BaseResponse;
import ru.baykalapps.base.model.Installations;
import ru.baykalapps.base.prefs.AppPrefs;
import ru.baykalapps.base.utils.ConverterUtil;
import ru.baykalapps.base.utils.UiUtil;


public class MainActivity extends BaseSpiceFragmentActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        StartFragment.OnStartPageVisibilityChangListener {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.inflateMenu(R.menu.main_menu);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                R.id.content,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int backStack = getSupportFragmentManager().getBackStackEntryCount();
                if (backStack == 0)
                    mNavigationDrawerFragment.toggle();
                else
                    onBackPressed();
            }
        });

        startFragment(new StartFragment(), false);

        invalidateForTheme(SinaxApplication.THEME);

        if (SinaxApplication.THEME == 1) {
            mToolbar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mToolbar.getNavigationIcon() != null) {
                        mToolbar.getNavigationIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
                        mToolbar.getNavigationIcon().invalidateSelf();
                    }
                }
            }, 50);
        }

        if (checkPlayServices() && !AppPrefs.getGcmRegistered()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

        getSpiceManager().addListenerIfPending(boolean.class, "RevisionRequest", new RevisionListener());
        getSpiceManager().execute(new InstallationsCountRequest(), new InstallationsCountListener());

        if (!AppPrefs.getDownloadPropositionWasShowed()) {
            ConfirmCacheDialog.show(this);
            AppPrefs.setDownloadPropositionWasShowed();
        }

    }


    public void reloadUsersCount() {
        ((TextView) mToolbar.findViewById(R.id.usersCount)).setText(
                getString(R.string.users_count_short,
                        AppPrefs.getInstallationsCount(),
                        AppPrefs.getUsersCount()));
    }

    @Override
    public void onNavigationDrawerItemSelected(long categoryId, String categoryName) {
        DataActivity.newInstance(this, categoryId, categoryName);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean status = false;
        if (!mNavigationDrawerFragment.isDrawerOpen()) {

            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            Fragment fragment = null;


            for (int i = fragments.size() - 1; i > 0; i--) {
                fragment = fragments.get(i);
                if (fragment != null)
                    break;
            }

            if (fragment != null && fragment instanceof BaseMenuFragment) {
                status = ((BaseMenuFragment) fragment).createMenu(menu, getMenuInflater());
            } else {
                getMenuInflater().inflate(R.menu.main_menu, menu);
                setTitle("");
                status = true;
            }
        }


        if (menu.size() == 0) {
            mToolbar.findViewById(R.id.action_bar_root_view).setPadding(0, 0, (int) ConverterUtil.dpToPix(this, 50), 0);
        } else {
            mToolbar.findViewById(R.id.action_bar_root_view).setPadding(0, 0, 0, 0);
        }

        return status || super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.auth || id == R.id.logout) {
            startActivity(new Intent(this, AuthActivity.class));
            return true;
        } else if (id == R.id.toolbar_switch_theme || id == R.id.toolbar_switch_theme_light) {
            switchTheme();
            return true;
        } else if (id == R.id.toolbar_switch_lang) {
            if (!SinaxApplication.TRANSLATION_ENABLED)
                return true;

            String lang = AppPrefs.getLanguage();
            switch (lang) {
                case "ru":
                    lang = "en";
                    break;
                case "en":
                    lang = "es";
                    break;
                case "es":
                    lang = "ru";
                    break;
            }
            AppPrefs.setLanguage(lang);
            UiUtil.switchLanguage(this, lang);

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }

        if (currentFragment != null) {
            if (currentFragment.onOptionsItemSelected(item))
                return true;
        }

        return validateClickByViewId(id) || super.onOptionsItemSelected(item);
    }

    private void switchTheme() {
        SinaxApplication.THEME = SinaxApplication.THEME == -1 ? 1 : -1;
        AppPrefs.setTheme(SinaxApplication.THEME);
        invalidateForTheme(SinaxApplication.THEME);
    }

    private void invalidateForTheme(int theme) {
        if (mToolbar.getNavigationIcon() == null) {
            return;
        }

        TextView titleTextView = ((TextView) mToolbar.findViewById(R.id.title));

        Drawable backgroundDrawable;
        Drawable toolbarDrawable;

        if (theme == -1 /* dark */) {
            toolbarDrawable = ContextCompat.getDrawable(this, R.drawable.toolbar_dark);

            titleTextView.setTextColor(Color.WHITE);
            ((TextView) mToolbar.findViewById(R.id.usersCount)).setTextColor(Color.WHITE);

            mToolbar.getNavigationIcon().setColorFilter(null);
            mToolbar.getNavigationIcon().invalidateSelf();

            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.background_dark);
        } else {

            toolbarDrawable = ContextCompat.getDrawable(this, R.drawable.toolbar_light);
            titleTextView.setTextColor(getResources().getColor(R.color.category_text_color));
            ((TextView) mToolbar.findViewById(R.id.usersCount)).setTextColor(getResources().getColor(R.color.category_text_color));

            mToolbar.getNavigationIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
            mToolbar.getNavigationIcon().invalidateSelf();

            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.background_light);
        }

        UiUtil.setBackgroundDrawable(findViewById(R.id.drawer_layout), backgroundDrawable);
        UiUtil.setBackgroundDrawable(mToolbar, toolbarDrawable);

        EventBus.getDefault().post(new ThemeChangeEvent());
        mNavigationDrawerFragment.changeTheme();

        supportInvalidateOptionsMenu();
    }


    public void onClick(View view) {
        validateClickByViewId(view.getId());
    }

    private boolean validateClickByViewId(int id) {
        if (id == R.id.toolbarAddNote) {
            startFragment(new NoteFragment(), true);
            return true;
        } else if (id == R.id.openNotes) {
            startFragment(NotesListFragment.newInstance(), true);
            return true;
        } else if (id == R.id.openMenu) {
            mNavigationDrawerFragment.toggle();
            return true;
        } else if (id == R.id.share) {
            startFragment(new ShareFragment(), true);
            return true;
        } else if (id == R.id.developer) {
            DataActivity.newInstance(this, DataActivity.ACTION_VIEW_DEVELOPER);
            return true;
        } else if (id == R.id.contacts) {
            startFragment(new ContactsFragment(), true);
            return true;
        } else if (id == R.id.hotline) {
            DataActivity.newInstance(this, DataActivity.ACTION_HOT_LINE);
            return true;
        }

        return false;
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        mToolbar.setTitle(title);
        ((TextView) mToolbar.findViewById(R.id.title)).setText(title);
    }

    @Override
    public void onStartPageVisibilityChanged(boolean visible) {
        mNavigationDrawerFragment.showDrawerToggle(visible);

        if (visible) {
            mToolbar.findViewById(R.id.title).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startFragment(new SettingFragment(), true);
                }
            });
            mToolbar.findViewById(R.id.usersCount).setVisibility(View.VISIBLE);
        } else {
            mToolbar.findViewById(R.id.title).setOnClickListener(null);
            mToolbar.findViewById(R.id.usersCount).setVisibility(View.GONE);
        }

    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @SuppressWarnings("unused")
    public void onEvent(ReceiveTokenEvent event) {

        String IMEI = ((TelephonyManager)
                getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

        if (IMEI == null)
            IMEI = Settings.Secure.ANDROID_ID;


        getSpiceManager().execute(new RegisterDeviceRequest(event.getToken(), IMEI), new RequestListener<BaseResponse>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                Log.i("RegisterDeviceRequest", "spiceException", spiceException);
                AppPrefs.setGcmRegistered(false);
            }

            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                Log.i("RegisterDeviceRequest", "baseResponse: " + baseResponse.getStatus());
                AppPrefs.setGcmRegistered(true);
            }
        });
    }

    //-------------------------
    private final class InstallationsCountListener extends SimplerRequestListener<Installations> {

        @Override
        public void onRequestSuccess(Installations installations) {
            reloadUsersCount();
        }
    }

    private final class RevisionListener implements PendingRequestListener<Boolean> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(Boolean revisionChanged) {
            Log.i("RevisionListener", "Revision " + revisionChanged);

            EventBus.getDefault().post(new RevisionCheckEvent());
        }

        @Override
        public void onRequestNotFound() {
            EventBus.getDefault().post(new RevisionCheckEvent());
        }
    }


}
