package ru.baykalapps.base.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;

import ru.baykalapps.base.Constant;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.activities.base.BaseSpiceFragmentActivity;
import ru.baykalapps.base.fragments.AboutDeveloper;
import ru.baykalapps.base.fragments.ChatFragment;
import ru.baykalapps.base.fragments.ViewCategoryFragment;
import ru.baykalapps.base.fragments.base.BaseMenuFragment;
import ru.baykalapps.base.model.Installations;
import ru.baykalapps.base.prefs.AppPrefs;
import ru.baykalapps.base.utils.ConverterUtil;
import ru.baykalapps.base.utils.UiUtil;

/**
 * Created by ovitali on 14.07.2015.
 * Project is Sinax
 */
public class DataActivity extends BaseSpiceFragmentActivity {

    public static void newInstance(Context context, long categoryId, String categoryName) {
        Intent intent = new Intent(context, DataActivity.class);
        intent.putExtra(Constant.PARAMS.CATEGORY_ID, categoryId);
        intent.putExtra(Constant.PARAMS.CATEGORY_NAME, categoryName);
        intent.putExtra("action", ACTION_VIEW_CATEGORY);

        context.startActivity(intent);
    }

    public static void newInstance(Context context, int action) {
        Intent intent = new Intent(context, DataActivity.class);
        intent.putExtra("action", action);
        context.startActivity(intent);
    }

    private Toolbar mToolbar;
    private BaseMenuFragment mMenuFragment;

    public final static int ACTION_VIEW_CATEGORY = 1;
    public final static int ACTION_VIEW_DEVELOPER = 3;
    public final static int ACTION_HOT_LINE = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int action = getIntent().getExtras().getInt("action");
        if (action != ACTION_HOT_LINE)
            setContentView(R.layout.activity_base);
        else
            setContentView(R.layout.activity_hotline);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (action == ACTION_VIEW_CATEGORY) {
            String categoryName = getIntent().getExtras().getString(Constant.PARAMS.CATEGORY_NAME);
            long categoryId = getIntent().getExtras().getLong(Constant.PARAMS.CATEGORY_ID);

            mMenuFragment = ViewCategoryFragment.newInstance(categoryId, categoryName);

        } else if (action == ACTION_HOT_LINE) {
            mMenuFragment = new ChatFragment();
            getSpiceManager().addListenerIfPending(Installations.class, "Installations", new InstallationsCountListener());
            ((TextView) mToolbar.findViewById(R.id.usersCount)).setTextColor(getResources().getColor(R.color.hot_line));
            reloadUsersCount();

        } else if (action == ACTION_VIEW_DEVELOPER) {
            mMenuFragment = new AboutDeveloper();

        }

        startFragment(mMenuFragment, false);

        invalidateForTheme(SinaxApplication.THEME);
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        mToolbar.setTitle(title);
        ((TextView) mToolbar.findViewById(R.id.title)).setText(title);
    }

    public void reloadUsersCount() {
        ((TextView) mToolbar.findViewById(R.id.usersCount)).setText(getString(R.string.users_count_full, AppPrefs.getInstallationsCount(), AppPrefs.getUsersCount()));
    }

    private void invalidateForTheme(int theme) {
        if (mToolbar.getNavigationIcon() == null) {
            return;
        }

        TextView titleTextView = ((TextView) mToolbar.findViewById(R.id.title));

        Drawable backgroundDrawable;
        Drawable toolbarDrawable;

        if (theme == -1 /* dark */) {
            toolbarDrawable = ContextCompat.getDrawable(this, R.drawable.toolbar_dark);

            titleTextView.setTextColor(Color.WHITE);

            mToolbar.getNavigationIcon().setColorFilter(null);
            mToolbar.getNavigationIcon().invalidateSelf();

            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.background_dark);
        } else {

            toolbarDrawable = ContextCompat.getDrawable(this, R.drawable.toolbar_light);
            titleTextView.setTextColor(getResources().getColor(R.color.category_text_color));

            mToolbar.getNavigationIcon().setColorFilter(SinaxApplication.GREY_COLOR, PorterDuff.Mode.SRC_IN);
            mToolbar.getNavigationIcon().invalidateSelf();

            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.background_light);
        }

        UiUtil.setBackgroundDrawable(findViewById(R.id.content), backgroundDrawable);
        UiUtil.setBackgroundDrawable(mToolbar, toolbarDrawable);

        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = mMenuFragment.createMenu(menu, getMenuInflater());
        if (menu.size() == 0) {
            mToolbar.findViewById(R.id.action_bar_root_view).setPadding(0, 0, (int) ConverterUtil.dpToPix(this, 50), 0);
        } else {
            mToolbar.findViewById(R.id.action_bar_root_view).setPadding(0, 0, 0, 0);
        }
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.auth || id == R.id.logout) {
            startActivity(new Intent(this, AuthActivity.class));
            return true;
        }

        return mMenuFragment.onOptionsItemSelected(item);
    }

    private final class InstallationsCountListener implements PendingRequestListener<Installations> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(Installations installations) {
            reloadUsersCount();
        }

        @Override
        public void onRequestNotFound() {

        }
    }
}
