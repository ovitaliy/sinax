package ru.baykalapps.base.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import ru.baykalapps.base.R;
import ru.baykalapps.base.activities.base.BaseSpiceFragmentActivity;
import ru.baykalapps.base.fragments.auth.LoginFragment;
import ru.baykalapps.base.fragments.auth.LogoutFragment;
import ru.baykalapps.base.fragments.auth.RegistrationFragment;
import ru.baykalapps.base.prefs.AuthPref;
import ru.baykalapps.base.utils.UiUtil;

public class AuthActivity extends BaseSpiceFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (AuthPref.getAuthToken() == null) {
            startFragment(R.id.container, new LoginFragment(), false, false);
        } else {
            startFragment(R.id.container, new LogoutFragment(), false, false);
        }


        Drawable backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.background_light);
        Drawable toolbarDrawable = ContextCompat.getDrawable(this, R.drawable.toolbar_dark);

        UiUtil.setBackgroundDrawable(findViewById(R.id.content), backgroundDrawable);
        UiUtil.setBackgroundDrawable(toolbar, toolbarDrawable);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void navigateToRegistration() {
        startFragment(R.id.container, new RegistrationFragment(), false, false);
    }

    public void navigateToLogin() {
        startFragment(R.id.container, new LoginFragment(), false, false);
    }


}
