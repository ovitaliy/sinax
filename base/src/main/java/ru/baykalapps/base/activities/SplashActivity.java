package ru.baykalapps.base.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.List;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.R;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.activities.base.BaseSpiceFragmentActivity;
import ru.baykalapps.base.api.requests.RevisionRequest;
import ru.baykalapps.base.api.responces.SimplerRequestListener;
import ru.baykalapps.base.events.RevisionCheckEvent;
import ru.baykalapps.base.model.CardImage;

public class SplashActivity extends BaseSpiceFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Waiter().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

        getSpiceManager().execute(new RevisionRequest(), "RevisionRequest", DurationInMillis.ONE_SECOND, new RevisionListener());
    }

    private class Waiter extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void[] params) {
            try {
                List<CardImage> cardImageList = SinaxApplication.getDatabaseDao().getCardImageDao().loadAll();
                SinaxApplication.PHOTO_CARDS = new CardImage[cardImageList.size()];

                SinaxApplication.PHOTO_CARDS = cardImageList.toArray(SinaxApplication.PHOTO_CARDS);

                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void o) {
            super.onPostExecute(o);

            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }
    }

    //-------------------------
    private final class RevisionListener extends SimplerRequestListener<Boolean> {

        @Override
        public void onRequestSuccess(Boolean revisionChanged) {
            Log.i("RevisionListener", "Revision " + revisionChanged);

            EventBus.getDefault().post(new RevisionCheckEvent());
        }
    }


}
