package ru.baykalapps.base.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import ru.baykalapps.base.Constant;
import ru.baykalapps.base.SinaxApplication;
import ru.baykalapps.base.db.dao.CategoryDao;
import ru.baykalapps.base.events.CacheProgressEvent;
import ru.baykalapps.base.model.Category;
import ru.baykalapps.base.prefs.AppPrefs;
import ru.baykalapps.base.utils.downloads.CacheFile;
import ru.baykalapps.base.utils.downloads.SaveImageRunnable;
import ru.baykalapps.base.utils.downloads.SaveYoutubeRunnable;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class CacheService extends Service {

    private static final String ACTION_CACHE = "ru.iappstee.sinax.services.action.cache";

    private CacheThread mCacheThread;

    private float mLastProgress;

    private static final boolean VIDEO_CACHE_REQUIRED = false;

    public static void startActionCache(Context context) {
        Intent intent = new Intent(context, CacheService.class);
        intent.setAction(ACTION_CACHE);
        context.startService(intent);
    }

    public CacheService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        handleActionCache();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void handleActionCache() {
        if (mCacheThread != null) {
            stopSelf();
            return;
        }

        mCacheThread = new CacheThread();
        mCacheThread.start();
    }

    private class CacheThread extends Thread {

        Pattern srcPattern = Pattern.compile("src=\"(.*?)\"");
        Matcher srcMatcher;

        public CacheThread() {
            setName("CacheThread");
        }

        @Override
        public void run() {

            String lang = AppPrefs.getLanguage();

            List<Category> categoryList = SinaxApplication.getDatabaseDao().getCategoryDao()
                    .queryBuilder().where(CategoryDao.Properties.LANG.eq(lang)).list();

            int i = 0;
            while (categoryList.size() == 0 && i++ < 50) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                categoryList = SinaxApplication.getDatabaseDao().getCategoryDao().loadAll();
            }

            Iterator<Category> categoryIterator = categoryList.iterator();
            while (categoryIterator.hasNext()) {
                Category category = categoryIterator.next();
                CacheFile cacheFile = new CacheFile(getApplicationContext(), category.getId());

                if (cacheFile.getHtmlCacheFile().exists()) {
                    categoryIterator.remove();
                }
            }

            EventBus.getDefault().post(new CacheProgressEvent(mLastProgress));

            int categoriesCount = categoryList.size();

            float progressPerCategory = 100.f / categoriesCount;

            for (i = 0; i < categoriesCount; i++) {
                try {

                    List<Runnable> runnables = new ArrayList<>();

                    CacheFile cacheFile = new CacheFile(getApplicationContext(), categoryList.get(i).getId());

                    URL url = new URL(String.format(Constant.CATEGORY_URL, categoryList.get(i).getId()));
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    String html = IOUtils.toString(conn.getInputStream());

                    if (!cacheFile.getHtmlCacheFile().exists())
                        cacheFile.getHtmlCacheFile().createNewFile();

                    IOUtils.copy(new StringReader(html), new FileOutputStream(cacheFile.getHtmlCacheFile()));

                    { // create runnable for all images
                        Pattern imagePattern = Pattern.compile("<img(.*?)>");
                        Matcher imageMatcher = imagePattern.matcher(html);

                        while (imageMatcher.find()) {
                            String imageTag = imageMatcher.group();
                            String imageUrl = getSrcFromTag(imageTag);

                            runnables.add(new SaveImageRunnable(imageUrl, cacheFile.getImageFile(imageUrl)));
                        }
                    }

                    if (VIDEO_CACHE_REQUIRED) { // create runnable for all youtubes
                        Pattern imagePattern = Pattern.compile("<iframe(.*?)>");
                        Matcher imageMatcher = imagePattern.matcher(html);

                        while (imageMatcher.find()) {
                            String iframeTag = imageMatcher.group();

                            String youtubeUrl = getSrcFromTag(iframeTag);

                            runnables.add(new SaveYoutubeRunnable(youtubeUrl, cacheFile.getYoutubeFile(youtubeUrl)));
                        }
                    }

                    float progressPerTask = progressPerCategory / runnables.size();

                    for (int ii = 0; ii < runnables.size(); ii++) {
                        runnables.get(ii).run();

                        float progress = progressPerCategory * i + progressPerTask * (ii + 1);

                        mLastProgress = progress;

                        EventBus.getDefault().post(new CacheProgressEvent(progress));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
            EventBus.getDefault().post(new CacheProgressEvent(100));
            stopSelf();
        }

        private String getSrcFromTag(String tag) {
            srcMatcher = srcPattern.matcher(tag);

            if (srcMatcher.find()) {
                String url = srcMatcher.group();

                url = url.replaceAll("\"", "");
                url = url.replace("src=", "");

                return url;
            }
            return null;
        }
    }
}

