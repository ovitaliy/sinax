package ru.baykalapps.bellator;

import ru.baykalapps.base.SinaxApplication;

/**
 * Created by ovitali on 15.09.2015.
 */
public class App extends SinaxApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        App.VISIT_CARD = R.drawable.visit_card;
        App.MAIN_LABEL = getString(R.string.app_name);
        App.APP_ICON = R.mipmap.ic_launcher;

        App.TRANSLATION_ENABLED = true;
    }
}
